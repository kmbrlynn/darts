#!/bin/bash
#
# Pipeline: dartconnect.com --> grok_data.sh --> generate_site.sh
#
# This script pulls down match report data from dartconnect.com and caches it
# in a gitignored directory for raw data
#
# It then prints a master output report to stdout, which is used as input
# for the next stage of the pipeline

set -euo pipefail
source data.sh
source util.sh

# Recap tabs: 'matches', 'games', 'players', 'counts'
# The players tab has someone's MPR and 3DA
BASE_URL="https://recap.dartconnect.com/players"

function set_up_directories () {
    rm -rf $RAW_DATA && mkdir -vp $RAW_DATA

    echo -e "$FUNCNAME: done\n" >&2
}

function curl_match_data () {
    curl 2>/dev/null ${BASE_URL}/$1 | \
        grep data-page | sed 's/&quot;/\n/g' > $RAW_DATA/${1}.txt
    echo "$FUNCNAME: wrote $RAW_DATA/${1}.txt" >&2
}

function sanitize_names () {
    for f in ${RAW_DATA}/*; do
        sed -i 's/Casey/C/g' ${f}
        sed -i 's/Douglas/D/g' ${f}
        sed -i 's/Meagan C/Meagan CL/g' ${f}
        sed -i 's/Meagan CLL/Meagan CL/g' ${f}
        sed -i 's/Hannah C/Hannah CL/g' ${f}
        sed -i 's/Katie L/Katie Lan/g' ${f}
        sed -i 's/Katie Lani/Katie Liv/g' ${f}
        sed -i 's/Katie Livv/Katie Liv/g' ${f}
        sed -i 's/Orellana/O/g' ${f}
    done

    echo "$FUNCNAME: done" >&2
}

function match_date () {
    date=$(cat ${RAW_DATA}/${1}.txt | grep -A2 server_match_start_date | tail -n1)
    time=$(cat ${RAW_DATA}/${1}.txt | grep -v server | grep -A2 match_start_date | tail -n1)
    echo "$FUNCNAME: $date, $time"
}

function match_type () {
    game=$(cat ${RAW_DATA}/${1}.txt | grep -A2 game_name | tail -n1)
    echo "$FUNCNAME: $game"
}

function player_averages_table () {
    cat ${RAW_DATA}/${1}.txt | grep -vE "show|match|dartconnect" | \
        grep -A999 players | grep -B999 PerGame
}

function match_players () {
    player_line=$(player_averages_table $1 | grep -A2 name | \
        grep -vE "name|:|--" | xargs)
    players=$(echo $player_line | sed "s/[^ ]$/& /;s/ \([^ ]*\) /\1 /g")

    echo "$FUNCNAME: $players"
}

function check_number () {
    printf "%g" "$(echo $1 | cut -d " " -f 1)" &> /dev/null
    [[ $? == 0 ]] && echo "$1" || echo "N/A"
}

function marks_per_round () {
    mpr=$(player_averages_table $1 | grep -A2 cricket_average | \
        grep -vE "cricket|:|--" | xargs)

    echo $FUNCNAME: $(check_number "$mpr")
}

function three_dart_average () {
    average_01=$(player_averages_table $1 | grep -A2 average_01 | \
        grep -vE "average_01|:|--" | xargs)

    echo $FUNCNAME: $(check_number "$average_01")
}

function win_percentage () {
    wins=$(player_averages_table $1 | grep -A1 win_percentage | \
        grep -vE "win_percentage|--" | cut -d ":" -f 2 | cut -d "," -f 1 | \
        xargs)

    echo $FUNCNAME: $(check_number "$wins")
}

function get_data () {
    for match in ${MATCHES[@]}; do
        curl_match_data $match
    done

    echo -e "$FUNCNAME: done\n" >&2
}

function match_report () {
    echo "${BASE_URL}/$1"
    match_date $1
    match_type $1
    match_players $1
    marks_per_round $1
    three_dart_average $1
    win_percentage $1
    echo

    echo "$FUNCNAME: $1 done" >&2
}

function report () {
    for match in ${MATCHES[@]}; do
        match_report $match
    done

    echo >&2
    echo "$FUNCNAME: done" >&2
}

# ======== main

# Set up a clean raw data ouput directory
set_up_directories

# Pull down dartconnect data for each match, grepping out the important stuff
get_data

# Make the names more unique
sanitize_names

# Print a consolidated, greppable report of all matches
report
