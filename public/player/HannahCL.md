# HannahCL

<title>HannahCL: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="HannahCL.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 6      | Marks Per Round: 0.6
| '01     | 5      | Points Per Round: 30.88
| Total   | 11     | Win Percentage: 45.45

#### Average Marks Per Round, Cricket: 0.6

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Mar 10 2024 6:13 PM: [Cricket with AllieG, AnneR, ClothildeC, HannahCL](https://recap.dartconnect.com/games/65ee352495718706c3a7f7f4)
1. Mar 10 2024 7:13 PM: [Cricket with HannahCL, AnneR](https://recap.dartconnect.com/games/65ee476495718706c3a8103b)
1. Mar 24 2024 5:46 PM: [Cricket with KimD, MeaganCL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600a6bc0ed8913ccd2a2788)
1. Mar 24 2024 7:31 PM: [Cricket with AllieG, AveryL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600c0580ed8913ccd2a480f)
1. May 19 2024 5:37 PM: [Cricket with HannahCL, MeaganCL](https://recap.dartconnect.com/games/664a79196147440e89edaba7)
1. May 23 2024 6:33 PM: [Cricket with ClothildeC, HannahCL, MeaganCL, QuinnL](https://recap.dartconnect.com/games/664fc99c6147440e89f11e57)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 30.88

<details><b><summary>Click for list of '01 matches</summary></b>

1. Mar 10 2024 5:45 PM: [301 SISO with HannahCL, QuinnL, AnneR, MeaganCL](https://recap.dartconnect.com/games/65ee2ec595718706c3a7efc5)
1. Mar 10 2024 6:50 PM: [301 SISO with AllieG, AnneR, HannahCL, KimD](https://recap.dartconnect.com/games/65ee3b6095718706c3a7ff92)
1. Mar 24 2024 6:43 PM: [501 SISO with AllieG, HannahCL, AveryL, MeaganCL](https://recap.dartconnect.com/games/6600b20d0ed8913ccd2a35fd)
1. May 19 2024 6:23 PM: [501 SISO with AllieG, HannahCL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/664a7eb26147440e89edb0df)
1. May 19 2024 7:17 PM: [501 SISO with AllieG, KimD, BeccaE, HannahCL](https://recap.dartconnect.com/games/664a8bda6147440e89edbcb5)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
