window.onload = function() {
    var Cricket = new CanvasJS.Chart("Cricket-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#4F81BC",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 1.0
            }, {
                x: 2,
                y: 0.6
            }, {
                x: 3,
                y: 0.7
            }, {
                x: 4,
                y: 1.2
            }, {
                x: 5,
                y: 0.6
            }, {
                x: 6,
                y: 0.3
            }, {
                x: 7,
                y: 1.0
            }, {
                x: 8,
                y: 2.3
            }, {
                x: 9,
                y: 0.9
            }, {
                x: 10,
                y: 1.1
            }, {
                x: 11,
                y: 0.7
            }, {
                x: 12,
                y: 0.7
            }, {
                x: 13,
                y: 1.9
            }, {
                x: 14,
                y: 0.9
            }, {
                x: 15,
                y: 1.4
            }, {
                x: 16,
                y: 0.6
            }, {
                x: 17,
                y: 0.8
            }, {
                x: 18,
                y: 0.4
            }, {
                x: 19,
                y: 1.0
            }, {
                x: 20,
                y: 1.1
            }, {
                x: 21,
                y: 0.9
            }, {
                x: 22,
                y: 0.8
            }, ],
        }, ],
        axisY: {
            title: "Marks Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 3,
        },
    });
    Cricket.render();
    var O1 = new CanvasJS.Chart("O1-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#C0504E",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 36.0
            }, {
                x: 2,
                y: 21.75
            }, {
                x: 3,
                y: 25.71
            }, {
                x: 4,
                y: 15.53
            }, {
                x: 5,
                y: 27.86
            }, {
                x: 6,
                y: 29.11
            }, {
                x: 7,
                y: 30.16
            }, {
                x: 8,
                y: 27.33
            }, {
                x: 9,
                y: 24.12
            }, {
                x: 10,
                y: 22.9
            }, {
                x: 11,
                y: 43.25
            }, {
                x: 12,
                y: 39.26
            }, {
                x: 13,
                y: 34.73
            }, {
                x: 14,
                y: 34.73
            }, {
                x: 15,
                y: 34.75
            }, {
                x: 16,
                y: 31.0
            }, {
                x: 17,
                y: 30.48
            }, {
                x: 18,
                y: 37.13
            }, {
                x: 19,
                y: 39.26
            }, {
                x: 20,
                y: 22.0
            }, {
                x: 21,
                y: 35.63
            }, {
                x: 22,
                y: 44.56
            }, ],
        }, ],
        axisY: {
            title: "Points Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 75,
        },
    });
    O1.render();
};