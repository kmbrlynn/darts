# SophiaR

<title>SophiaR: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="SophiaR.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 22     | Marks Per Round: 0.95
| '01     | 22     | Points Per Round: 31.24
| Total   | 44     | Win Percentage: 47.73

#### Average Marks Per Round, Cricket: 0.95

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 25 2024 5:26 PM: [Cricket with AveryL, SophiaR](https://recap.dartconnect.com/games/65dbc41595718706c397934b)
1. Mar 10 2024 5:49 PM: [Cricket with AveryL, SophiaR, AllieG, ClothildeC](https://recap.dartconnect.com/games/65ee2dc095718706c3a7ee63)
1. Mar 10 2024 6:48 PM: [Cricket with MeaganCL, SophiaR, AveryL, QuinnL](https://recap.dartconnect.com/games/65ee412195718706c3a807da)
1. Mar 24 2024 5:46 PM: [Cricket with KimD, MeaganCL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600a6bc0ed8913ccd2a2788)
1. Mar 24 2024 6:38 PM: [Cricket with KatieLan, KimD, JustineB, SophiaR](https://recap.dartconnect.com/games/6600b14d0ed8913ccd2a3516)
1. Mar 24 2024 7:31 PM: [Cricket with AllieG, AveryL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600c0580ed8913ccd2a480f)
1. Mar 31 2024 8:00 PM: [Cricket with KatieR, SophiaR, KimD, QuinnL](https://recap.dartconnect.com/games/6609fd280ed8913ccd318339)
1. Mar 31 2024 8:23 PM: [Cricket with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/660a029c0ed8913ccd318936)
1. Apr 14 2024 6:49 PM: [Cricket with QuinnL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/661c63920ed8913ccd404ed3)
1. Apr 14 2024 8:12 PM: [Cricket with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c74f80ed8913ccd406087)
1. Apr 21 2024 6:59 PM: [Cricket with SophiaR, SylvieC, KatieR, KimD](https://recap.dartconnect.com/games/6625a0106147440e89d2a2ad)
1. Apr 21 2024 7:28 PM: [Cricket with ClothildeC, KatieR, KatieLiv, SophiaR](https://recap.dartconnect.com/games/6625a64c6147440e89d2a946)
1. Apr 28 2024 7:32 PM: [Cricket with AllieG, KimD, KatieR, SophiaR](https://recap.dartconnect.com/games/662eded76147440e89d9471a)
1. Apr 28 2024 7:45 PM: [Cricket with AllieG, SophiaR](https://recap.dartconnect.com/games/662ee58a6147440e89d94dc4)
1. May 05 2024 6:06 PM: [Cricket with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/6638059b6147440e89e10f02)
1. May 05 2024 6:37 PM: [Cricket with AveryL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/663813296147440e89e11c47)
1. May 26 2024 8:14 PM: [Cricket with KimD, SophiaR, AveryL, KatieLiv](https://recap.dartconnect.com/games/6653d5766147440e89f37f16)
1. Jun 09 2024 6:10 PM: [Cricket with KatieR, SophiaR, AllieG, KimD](https://recap.dartconnect.com/games/66662bfe6147440e890022e4)
1. Jun 09 2024 7:10 PM: [Cricket with AllieG, ClothildeC, QuinnL, SophiaR](https://recap.dartconnect.com/games/666639666147440e89003236)
1. Jul 01 2024 6:19 PM: [Cricket with SophiaR, KimD](https://recap.dartconnect.com/games/66832ebefab9ed7c6c8404d4)
1. Jul 01 2024 6:57 PM: [Cricket with SophiaR, QuinnL](https://recap.dartconnect.com/games/66833830fab9ed7c6c841164)
1. Jul 01 2024 7:25 PM: [Cricket with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/66833ef3fab9ed7c6c841cbc)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 31.24

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 25 2024 5:56 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbc84d95718706c39797d2)
1. Feb 25 2024 6:11 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbcb0695718706c3979b1d)
1. Feb 25 2024 6:27 PM: [401 SISO with QuinnL, SophiaR, KatieR, MichaelaK](https://recap.dartconnect.com/games/65dbd07795718706c397a1dc)
1. Mar 10 2024 5:10 PM: [301 SIDO with SophiaR, KimD](https://recap.dartconnect.com/games/65ee262695718706c3a7e563)
1. Mar 10 2024 5:31 PM: [401 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65ee29fb95718706c3a7e9ce)
1. Mar 24 2024 6:19 PM: [301 SISO with SophiaR, MeaganCL](https://recap.dartconnect.com/games/6600a8ed0ed8913ccd2a2a5c)
1. Mar 31 2024 7:44 PM: [401 SISO with QuinnL, SophiaR, KatieLiv, KatieR](https://recap.dartconnect.com/games/6609f8820ed8913ccd317e77)
1. Apr 14 2024 7:20 PM: [401 SISO with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c675a0ed8913ccd405282)
1. Apr 14 2024 7:37 PM: [201 SISO with SophiaR, QuinnL](https://recap.dartconnect.com/games/661c69fe0ed8913ccd405551)
1. Apr 14 2024 7:45 PM: [301 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/661c6c180ed8913ccd4057c0)
1. Apr 14 2024 7:57 PM: [401 SISO with KatieLiv, QuinnL, KimD, SophiaR](https://recap.dartconnect.com/games/661c6f4f0ed8913ccd405ad5)
1. Apr 21 2024 6:45 PM: [301 SISO with SophiaR, KimD](https://recap.dartconnect.com/games/662598b76147440e89d29acc)
1. Apr 21 2024 7:56 PM: [401 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/6625a9d16147440e89d2acc3)
1. Apr 21 2024 8:26 PM: [301 SISO with SophiaR, KimD](https://recap.dartconnect.com/games/6625b04f6147440e89d2b3f9)
1. Apr 28 2024 7:12 PM: [501 SISO with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/662edb656147440e89d9435a)
1. May 05 2024 6:19 PM: [401 SISO with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/663808126147440e89e11159)
1. May 05 2024 7:22 PM: [501 SISO with AllieG, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/663817c76147440e89e120ca)
1. May 05 2024 7:42 PM: [401 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/66381aac6147440e89e123da)
1. Jun 09 2024 5:54 PM: [301 SISO with KimD, SophiaR](https://recap.dartconnect.com/games/6666262d6147440e89001cbe)
1. Jun 09 2024 6:56 PM: [401 SISO with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/666635776147440e89002d96)
1. Jul 01 2024 6:37 PM: [301 SISO with DebbieR, SophiaR](https://recap.dartconnect.com/games/66833133fab9ed7c6c8407ff)
1. Jul 01 2024 6:46 PM: [401 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/66833300fab9ed7c6c840a52)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
