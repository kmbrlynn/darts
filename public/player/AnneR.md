# AnneR

<title>AnneR: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="AnneR.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 7      | Marks Per Round: 0.66
| '01     | 7      | Points Per Round: 29.14
| Total   | 14     | Win Percentage: 64.29

#### Average Marks Per Round, Cricket: 0.66

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 6:04 PM: [Cricket with AnneR, MegL, MDD, MichaelaK](https://recap.dartconnect.com/games/65d2943e95718706c38f3bd4)
1. Mar 10 2024 6:13 PM: [Cricket with AllieG, AnneR, ClothildeC, HannahCL](https://recap.dartconnect.com/games/65ee352495718706c3a7f7f4)
1. Mar 10 2024 7:13 PM: [Cricket with HannahCL, AnneR](https://recap.dartconnect.com/games/65ee476495718706c3a8103b)
1. Jun 24 2024 6:18 PM: [Cricket with MaryM, QuinnL, AnneR, KimD](https://recap.dartconnect.com/games/6679f4b6fab9ed7c6c7b7836)
1. Jun 24 2024 6:46 PM: [Cricket with AnneR, ClothildeC, MaryM, MeaganCL](https://recap.dartconnect.com/games/6679fbb2fab9ed7c6c7b81b5)
1. Jun 24 2024 7:28 PM: [Cricket with AllieG, ClothildeC, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a035afab9ed7c6c7b8e91)
1. Jun 24 2024 8:08 PM: [Cricket with AllieG, KimD, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a0de2fab9ed7c6c7ba2ea)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 29.14

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:53 PM: [401 SISO with AnneR, KimD, MeaganCL, MegL](https://recap.dartconnect.com/games/65d29b9d95718706c38f45b9)
1. Feb 18 2024 7:30 PM: [401 SISO with AnneR, KatieR, MDD, MeaganCL](https://recap.dartconnect.com/games/65d2a4c995718706c38f51ab)
1. Mar 10 2024 5:45 PM: [301 SISO with HannahCL, QuinnL, AnneR, MeaganCL](https://recap.dartconnect.com/games/65ee2ec595718706c3a7efc5)
1. Mar 10 2024 6:50 PM: [301 SISO with AllieG, AnneR, HannahCL, KimD](https://recap.dartconnect.com/games/65ee3b6095718706c3a7ff92)
1. Jun 24 2024 7:12 PM: [401 SISO with AllieG, MaryM, AnneR, QuinnL](https://recap.dartconnect.com/games/667a000dfab9ed7c6c7b88b5)
1. Jun 24 2024 7:44 PM: [401 SISO with KatieR, MeaganCL, AnneR, MaryM](https://recap.dartconnect.com/games/667a0794fab9ed7c6c7b96a3)
1. Jun 24 2024 8:43 PM: [401 SISO with AllieG, MeaganCL, AnneR, QuinnL](https://recap.dartconnect.com/games/667a15e1fab9ed7c6c7bb218)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
