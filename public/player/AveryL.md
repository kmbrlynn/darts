# AveryL

<title>AveryL: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="AveryL.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 21     | Marks Per Round: 1.0
| '01     | 15     | Points Per Round: 33.03
| Total   | 36     | Win Percentage: 55.56

#### Average Marks Per Round, Cricket: 1.0

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 25 2024 4:57 PM: [Cricket with AveryL, AllieG](https://recap.dartconnect.com/games/65dbbd9b95718706c3978be9)
1. Feb 25 2024 5:26 PM: [Cricket with AveryL, SophiaR](https://recap.dartconnect.com/games/65dbc41595718706c397934b)
1. Feb 25 2024 6:43 PM: [Cricket with AveryL, ChristianaL, AllieG, MDD](https://recap.dartconnect.com/games/65dbd6cb95718706c397a96d)
1. Mar 10 2024 5:08 PM: [Cricket with AveryL, AllieG](https://recap.dartconnect.com/games/65ee264495718706c3a7e583)
1. Mar 10 2024 5:49 PM: [Cricket with AveryL, SophiaR, AllieG, ClothildeC](https://recap.dartconnect.com/games/65ee2dc095718706c3a7ee63)
1. Mar 10 2024 6:13 PM: [Cricket with AveryL, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65ee371a95718706c3a7fa48)
1. Mar 10 2024 6:48 PM: [Cricket with MeaganCL, SophiaR, AveryL, QuinnL](https://recap.dartconnect.com/games/65ee412195718706c3a807da)
1. Mar 10 2024 7:32 PM: [Cricket with AllieG, AveryL](https://recap.dartconnect.com/games/65ee489795718706c3a811d3)
1. Mar 24 2024 5:54 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600a6e60ed8913ccd2a27ce)
1. Mar 24 2024 6:22 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600abfb0ed8913ccd2a2e28)
1. Mar 24 2024 7:31 PM: [Cricket with AllieG, AveryL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600c0580ed8913ccd2a480f)
1. May 05 2024 5:15 PM: [Cricket with AllieG, AveryL](https://recap.dartconnect.com/games/6637fde06147440e89e10737)
1. May 05 2024 6:06 PM: [Cricket with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/6638059b6147440e89e10f02)
1. May 05 2024 6:37 PM: [Cricket with AveryL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/663813296147440e89e11c47)
1. May 12 2024 6:47 PM: [Cricket with AllieG, KimD, AveryL, QuinnL](https://recap.dartconnect.com/games/66414b9d6147440e89e72fcc)
1. May 12 2024 7:19 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664150706147440e89e733fb)
1. May 12 2024 7:29 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664154756147440e89e7375e)
1. May 26 2024 8:14 PM: [Cricket with KimD, SophiaR, AveryL, KatieLiv](https://recap.dartconnect.com/games/6653d5766147440e89f37f16)
1. May 26 2024 8:39 PM: [Cricket with AllieG, ClothildeC, AveryL, QuinnL](https://recap.dartconnect.com/games/6653db066147440e89f3842d)
1. May 26 2024 9:03 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e0e06147440e89f388f0)
1. May 26 2024 9:27 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e6616147440e89f38d63)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 33.03

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 25 2024 5:56 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbc84d95718706c39797d2)
1. Feb 25 2024 6:11 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbcb0695718706c3979b1d)
1. Feb 25 2024 6:28 PM: [401 SISO with ChristianaL, KimD, AveryL, MDD](https://recap.dartconnect.com/games/65dbd01695718706c397a150)
1. Mar 10 2024 5:31 PM: [401 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65ee29fb95718706c3a7e9ce)
1. Mar 24 2024 5:19 PM: [301 SISO with AllieG, AveryL](https://recap.dartconnect.com/games/66009c110ed8913ccd2a1a77)
1. Mar 24 2024 5:35 PM: [501 SISO with AveryL, QuinnL, AllieG, BeccaE](https://recap.dartconnect.com/games/6600a0b60ed8913ccd2a200d)
1. Mar 24 2024 6:43 PM: [501 SISO with AllieG, HannahCL, AveryL, MeaganCL](https://recap.dartconnect.com/games/6600b20d0ed8913ccd2a35fd)
1. Mar 24 2024 7:15 PM: [501 SISO with AveryL, JustineB, AllieG, KatieLan](https://recap.dartconnect.com/games/6600b6cf0ed8913ccd2a3c38)
1. May 05 2024 6:19 PM: [401 SISO with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/663808126147440e89e11159)
1. May 05 2024 7:22 PM: [401 SISO with ClothildeC, KatieLiv, AveryL, MeaganCL](https://recap.dartconnect.com/games/6638183c6147440e89e1213e)
1. May 05 2024 7:58 PM: [301 SISO with AllieG, AveryL](https://recap.dartconnect.com/games/66381f6e6147440e89e128ec)
1. May 12 2024 5:39 PM: [301 SISO with AllieG, AveryL](https://recap.dartconnect.com/games/664138336147440e89e721c1)
1. May 12 2024 6:18 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/6641424c6147440e89e72891)
1. May 12 2024 6:28 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/664144cb6147440e89e72a48)
1. May 12 2024 7:49 PM: [401 SISO with AveryL, BeccaE, AllieG, QuinnL](https://recap.dartconnect.com/games/6641577f6147440e89e73a23)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
