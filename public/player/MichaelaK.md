# MichaelaK

<title>MichaelaK: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="MichaelaK.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 3      | Marks Per Round: 0.9
| '01     | 2      | Points Per Round: 26.43
| Total   | 5      | Win Percentage: 30.0

#### Average Marks Per Round, Cricket: 0.9

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 5:33 PM: [Cricket with MDD, MichaelaK](https://recap.dartconnect.com/games/65d28b9695718706c38f3068)
1. Feb 18 2024 6:04 PM: [Cricket with AnneR, MegL, MDD, MichaelaK](https://recap.dartconnect.com/games/65d2943e95718706c38f3bd4)
1. Feb 18 2024 7:28 PM: [Cricket with ClothildeC, QuinnL, KimD, MichaelaK](https://recap.dartconnect.com/games/65d2a36495718706c38f4fe6)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 26.43

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:38 PM: [401 SISO with KatieR, MichaelaK, MDD, QuinnL](https://recap.dartconnect.com/games/65d297d995718706c38f408b)
1. Feb 25 2024 6:27 PM: [401 SISO with QuinnL, SophiaR, KatieR, MichaelaK](https://recap.dartconnect.com/games/65dbd07795718706c397a1dc)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
