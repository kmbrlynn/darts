# SylvieC

<title>SylvieC: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="SylvieC.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 4      | Marks Per Round: 0.9
| '01     | 6      | Points Per Round: 35.47
| Total   | 10     | Win Percentage: 40.0

#### Average Marks Per Round, Cricket: 0.9

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Apr 21 2024 6:59 PM: [Cricket with SophiaR, SylvieC, KatieR, KimD](https://recap.dartconnect.com/games/6625a0106147440e89d2a2ad)
1. Jun 06 2024 6:48 PM: [Cricket with KatieR, PamO, KimD, SylvieC](https://recap.dartconnect.com/games/666240906147440e89fcc433)
1. Jun 06 2024 7:21 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/666247a06147440e89fccdbd)
1. Jun 06 2024 7:39 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/66624b816147440e89fcd3a1)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 35.47

<details><b><summary>Click for list of '01 matches</summary></b>

1. Apr 21 2024 6:04 PM: [401 SISO with KatieLiv, KatieR, StaceyE, SylvieC](https://recap.dartconnect.com/games/662590d16147440e89d29244)
1. Apr 21 2024 6:47 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, SylvieC](https://recap.dartconnect.com/games/6625995c6147440e89d29b85)
1. Apr 21 2024 7:29 PM: [301 SISO with KimD, SylvieC](https://recap.dartconnect.com/games/6625a2ee6147440e89d2a5c3)
1. Apr 21 2024 7:39 PM: [301 SIDO with KimD, SylvieC](https://recap.dartconnect.com/games/6625a5446147440e89d2a828)
1. Jun 06 2024 6:31 PM: [301 SISO with SylvieC, KimD](https://recap.dartconnect.com/games/66623b466147440e89fcbe8d)
1. Jun 06 2024 7:06 PM: [501 SISO with KatieR, MeaganCL, PamO, SylvieC](https://recap.dartconnect.com/games/666242dc6147440e89fcc719)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
