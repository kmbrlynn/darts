# MaryM

<title>MaryM: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="MaryM.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 4      | Marks Per Round: 1.43
| '01     | 4      | Points Per Round: 44.84
| Total   | 8      | Win Percentage: 62.5

#### Average Marks Per Round, Cricket: 1.43

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Jun 24 2024 6:18 PM: [Cricket with MaryM, QuinnL, AnneR, KimD](https://recap.dartconnect.com/games/6679f4b6fab9ed7c6c7b7836)
1. Jun 24 2024 6:46 PM: [Cricket with AnneR, ClothildeC, MaryM, MeaganCL](https://recap.dartconnect.com/games/6679fbb2fab9ed7c6c7b81b5)
1. Jun 24 2024 7:27 PM: [Cricket with KimD, MaryM, KatieR, QuinnL](https://recap.dartconnect.com/games/667a0317fab9ed7c6c7b8e04)
1. Jun 24 2024 8:07 PM: [Cricket with KatieR, MaryM, ClothildeC, QuinnL](https://recap.dartconnect.com/games/667a0e79fab9ed7c6c7ba405)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 44.84

<details><b><summary>Click for list of '01 matches</summary></b>

1. Jun 24 2024 7:12 PM: [401 SISO with AllieG, MaryM, AnneR, QuinnL](https://recap.dartconnect.com/games/667a000dfab9ed7c6c7b88b5)
1. Jun 24 2024 7:44 PM: [401 SISO with KatieR, MeaganCL, AnneR, MaryM](https://recap.dartconnect.com/games/667a0794fab9ed7c6c7b96a3)
1. Jun 24 2024 8:38 PM: [301 SISO with MaryM, KimD](https://recap.dartconnect.com/games/667a12a0fab9ed7c6c7babf2)
1. Jun 24 2024 8:45 PM: [301 SISO with KimD, MaryM](https://recap.dartconnect.com/games/667a14c7fab9ed7c6c7bb03c)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
