# KimD

<title>KimD: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="KimD.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 59     | Marks Per Round: 1.29
| '01     | 43     | Points Per Round: 36.99
| Total   | 102    | Win Percentage: 49.02

#### Average Marks Per Round, Cricket: 1.29

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 5:08 PM: [No Points with KatieR, KimD](https://recap.dartconnect.com/games/65d282d295718706c38f2520)
1. Feb 18 2024 5:24 PM: [Cricket with KatieR, KimD](https://recap.dartconnect.com/games/65d2868a95718706c38f29d3)
1. Feb 18 2024 5:40 PM: [Cricket with MegL, KatieR, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65d289f695718706c38f2e2e)
1. Feb 18 2024 7:28 PM: [Cricket with ClothildeC, QuinnL, KimD, MichaelaK](https://recap.dartconnect.com/games/65d2a36495718706c38f4fe6)
1. Feb 25 2024 5:09 PM: [Cricket with KatieR, KimD](https://recap.dartconnect.com/games/65dbbe5195718706c3978caf)
1. Feb 25 2024 5:28 PM: [Cricket with AllieG, ClothildeC, KatieR, KimD](https://recap.dartconnect.com/games/65dbc30c95718706c397921a)
1. Mar 10 2024 6:13 PM: [Cricket with AveryL, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65ee371a95718706c3a7fa48)
1. Mar 24 2024 5:46 PM: [Cricket with KimD, MeaganCL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600a6bc0ed8913ccd2a2788)
1. Mar 24 2024 6:38 PM: [Cricket with KatieLan, KimD, JustineB, SophiaR](https://recap.dartconnect.com/games/6600b14d0ed8913ccd2a3516)
1. Mar 24 2024 7:49 PM: [Cricket with ClothildeC, MeaganCL, BeccaE, KimD](https://recap.dartconnect.com/games/6600bfe00ed8913ccd2a477a)
1. Mar 31 2024 6:44 PM: [Cricket with ClothildeC, KatieLiv, KatieR, KimD](https://recap.dartconnect.com/games/6609eb160ed8913ccd31721b)
1. Mar 31 2024 8:00 PM: [Cricket with KatieR, SophiaR, KimD, QuinnL](https://recap.dartconnect.com/games/6609fd280ed8913ccd318339)
1. Mar 31 2024 8:23 PM: [Cricket with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/660a029c0ed8913ccd318936)
1. Apr 06 2024 4:56 PM: [Cricket with KimD, KatieR](https://recap.dartconnect.com/games/6611bcf80ed8913ccd37d264)
1. Apr 06 2024 5:22 PM: [Cricket with ClothildeC, KimD](https://recap.dartconnect.com/games/6611bf990ed8913ccd37d5d1)
1. Apr 14 2024 5:37 PM: [Cricket with QuinnL, KimD](https://recap.dartconnect.com/games/661c51610ed8913ccd403c7a)
1. Apr 14 2024 6:03 PM: [Cricket with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c55b00ed8913ccd4040e1)
1. Apr 14 2024 6:49 PM: [Cricket with QuinnL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/661c63920ed8913ccd404ed3)
1. Apr 14 2024 8:12 PM: [Cricket with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c74f80ed8913ccd406087)
1. Apr 21 2024 6:07 PM: [Cricket with KimD, ClothildeC](https://recap.dartconnect.com/games/6625901c6147440e89d29190)
1. Apr 21 2024 6:27 PM: [Cricket with KimD, ClothildeC](https://recap.dartconnect.com/games/6625952a6147440e89d296d8)
1. Apr 21 2024 6:59 PM: [Cricket with SophiaR, SylvieC, KatieR, KimD](https://recap.dartconnect.com/games/6625a0106147440e89d2a2ad)
1. Apr 28 2024 5:25 PM: [Cricket with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec3916147440e89d92c65)
1. Apr 28 2024 6:39 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/662ed5b46147440e89d93da8)
1. Apr 28 2024 7:32 PM: [Cricket with AllieG, KimD, KatieR, SophiaR](https://recap.dartconnect.com/games/662eded76147440e89d9471a)
1. May 05 2024 5:44 PM: [Cricket with MeaganCL, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/6638033a6147440e89e10ca0)
1. May 05 2024 6:37 PM: [Cricket with AveryL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/663813296147440e89e11c47)
1. May 12 2024 5:15 PM: [Cricket with ClothildeC, KimD](https://recap.dartconnect.com/games/664133246147440e89e71e6a)
1. May 12 2024 6:00 PM: [Cricket with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413fa66147440e89e726c4)
1. May 12 2024 6:18 PM: [Cricket with QuinnL, KimD](https://recap.dartconnect.com/games/6641435e6147440e89e72936)
1. May 12 2024 6:47 PM: [Cricket with AllieG, KimD, AveryL, QuinnL](https://recap.dartconnect.com/games/66414b9d6147440e89e72fcc)
1. May 12 2024 7:19 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664150706147440e89e733fb)
1. May 12 2024 7:29 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664154756147440e89e7375e)
1. May 19 2024 5:37 PM: [Cricket with AllieG, KimD, BeccaE, QuinnL](https://recap.dartconnect.com/games/664a74666147440e89eda7bb)
1. May 19 2024 6:37 PM: [Cricket with KimD, MeaganCL, AllieG, BeccaE](https://recap.dartconnect.com/games/664a88266147440e89edb96d)
1. May 23 2024 7:40 PM: [Cricket with QuinnL, KimD](https://recap.dartconnect.com/games/664fd6616147440e89f12db7)
1. May 26 2024 8:14 PM: [Cricket with KimD, SophiaR, AveryL, KatieLiv](https://recap.dartconnect.com/games/6653d5766147440e89f37f16)
1. May 26 2024 9:03 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e0e06147440e89f388f0)
1. May 26 2024 9:27 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e6616147440e89f38d63)
1. Jun 06 2024 6:48 PM: [Cricket with KatieR, PamO, KimD, SylvieC](https://recap.dartconnect.com/games/666240906147440e89fcc433)
1. Jun 06 2024 7:41 PM: [Cricket with KimD, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66624cb16147440e89fcd5b7)
1. Jun 06 2024 8:21 PM: [Cricket with PamO, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/666254e76147440e89fce370)
1. Jun 06 2024 8:58 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625cf56147440e89fcf0d3)
1. Jun 06 2024 9:06 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625f2d6147440e89fcf45b)
1. Jun 09 2024 5:22 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/666622406147440e89001862)
1. Jun 09 2024 6:10 PM: [Cricket with KatieR, SophiaR, AllieG, KimD](https://recap.dartconnect.com/games/66662bfe6147440e890022e4)
1. Jun 09 2024 7:10 PM: [Cricket with KimD, KatieR](https://recap.dartconnect.com/games/666638b46147440e89003154)
1. Jun 24 2024 6:18 PM: [Cricket with MaryM, QuinnL, AnneR, KimD](https://recap.dartconnect.com/games/6679f4b6fab9ed7c6c7b7836)
1. Jun 24 2024 6:43 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/6679fa8bfab9ed7c6c7b7fe8)
1. Jun 24 2024 7:27 PM: [Cricket with KimD, MaryM, KatieR, QuinnL](https://recap.dartconnect.com/games/667a0317fab9ed7c6c7b8e04)
1. Jun 24 2024 8:08 PM: [Cricket with AllieG, KimD, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a0de2fab9ed7c6c7ba2ea)
1. Jun 24 2024 9:02 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/667a1a48fab9ed7c6c7bba4d)
1. Jun 24 2024 9:17 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/667a1eabfab9ed7c6c7bc203)
1. Jul 01 2024 6:19 PM: [Cricket with SophiaR, KimD](https://recap.dartconnect.com/games/66832ebefab9ed7c6c8404d4)
1. Jul 01 2024 6:36 PM: [Cricket with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/668332dcfab9ed7c6c840a24)
1. Jul 01 2024 7:25 PM: [Cricket with KimD, DebbieR](https://recap.dartconnect.com/games/66833efefab9ed7c6c841cd8)
1. Jul 01 2024 8:01 PM: [Cricket with AllieG, KimD, DebbieR, QuinnL](https://recap.dartconnect.com/games/6683463efab9ed7c6c842a77)
1. Jul 01 2024 9:04 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/6683574afab9ed7c6c844c33)
1. Jul 01 2024 9:42 PM: [Cricket with AllieG, KimD, ClothildeC, QuinnL](https://recap.dartconnect.com/games/66835e3afab9ed7c6c8457de)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 36.99

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:53 PM: [401 SISO with AnneR, KimD, MeaganCL, MegL](https://recap.dartconnect.com/games/65d29b9d95718706c38f45b9)
1. Feb 25 2024 5:56 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbc84d95718706c39797d2)
1. Feb 25 2024 6:11 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbcb0695718706c3979b1d)
1. Feb 25 2024 6:28 PM: [401 SISO with ChristianaL, KimD, AveryL, MDD](https://recap.dartconnect.com/games/65dbd01695718706c397a150)
1. Mar 10 2024 5:10 PM: [301 SIDO with SophiaR, KimD](https://recap.dartconnect.com/games/65ee262695718706c3a7e563)
1. Mar 10 2024 5:31 PM: [401 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65ee29fb95718706c3a7e9ce)
1. Mar 10 2024 6:41 PM: [201 SISO with AllieG, KimD](https://recap.dartconnect.com/games/65ee386e95718706c3a7fc01)
1. Mar 10 2024 6:50 PM: [301 SISO with AllieG, AnneR, HannahCL, KimD](https://recap.dartconnect.com/games/65ee3b6095718706c3a7ff92)
1. Mar 24 2024 5:22 PM: [301 SIDO with KimD, ClothildeC](https://recap.dartconnect.com/games/66009b180ed8913ccd2a190b)
1. Mar 31 2024 5:55 PM: [401 SISO with ClothildeC, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6609dde00ed8913ccd316625)
1. Mar 31 2024 7:05 PM: [401 SISO with KatieLiv, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/6609ef380ed8913ccd3175ca)
1. Apr 14 2024 5:27 PM: [301 SISO with QuinnL, KimD](https://recap.dartconnect.com/games/661c4c080ed8913ccd403763)
1. Apr 14 2024 6:17 PM: [401 SISO with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c583a0ed8913ccd4043b3)
1. Apr 14 2024 6:32 PM: [401 SISO with ClothildeC, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/661c5bc90ed8913ccd404707)
1. Apr 14 2024 7:20 PM: [401 SISO with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c675a0ed8913ccd405282)
1. Apr 14 2024 7:57 PM: [401 SISO with KatieLiv, QuinnL, KimD, SophiaR](https://recap.dartconnect.com/games/661c6f4f0ed8913ccd405ad5)
1. Apr 21 2024 6:45 PM: [301 SISO with SophiaR, KimD](https://recap.dartconnect.com/games/662598b76147440e89d29acc)
1. Apr 21 2024 7:29 PM: [301 SISO with KimD, SylvieC](https://recap.dartconnect.com/games/6625a2ee6147440e89d2a5c3)
1. Apr 21 2024 7:39 PM: [301 SIDO with KimD, SylvieC](https://recap.dartconnect.com/games/6625a5446147440e89d2a828)
1. Apr 21 2024 8:26 PM: [301 SISO with SophiaR, KimD](https://recap.dartconnect.com/games/6625b04f6147440e89d2b3f9)
1. Apr 28 2024 5:49 PM: [401 SISO with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec5bf6147440e89d92e47)
1. Apr 28 2024 5:58 PM: [501 SISO with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/662ec8356147440e89d9304b)
1. May 05 2024 5:20 PM: [301 SISO with QuinnL, KimD](https://recap.dartconnect.com/games/6637f9636147440e89e10321)
1. May 05 2024 6:12 PM: [501 SISO with KatieLiv, QuinnL, KimD, MeaganCL](https://recap.dartconnect.com/games/663807ce6147440e89e11110)
1. May 05 2024 7:22 PM: [501 SISO with AllieG, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/663817c76147440e89e120ca)
1. May 12 2024 5:44 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/664139e46147440e89e722e9)
1. May 12 2024 5:52 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413ba06147440e89e72419)
1. May 19 2024 5:16 PM: [301 SISO with KimD, BeccaE](https://recap.dartconnect.com/games/664a6e326147440e89eda263)
1. May 19 2024 5:57 PM: [501 SISO with AllieG, QuinnL, BeccaE, KimD](https://recap.dartconnect.com/games/664a78f06147440e89edab7e)
1. May 19 2024 7:17 PM: [501 SISO with AllieG, KimD, BeccaE, HannahCL](https://recap.dartconnect.com/games/664a8bda6147440e89edbcb5)
1. Jun 06 2024 6:31 PM: [301 SISO with SylvieC, KimD](https://recap.dartconnect.com/games/66623b466147440e89fcbe8d)
1. Jun 06 2024 7:08 PM: [401 SISO with QuinnL, KimD](https://recap.dartconnect.com/games/666244096147440e89fcc88c)
1. Jun 06 2024 8:03 PM: [501 SISO with KatieR, MeaganCL, KimD, PamO](https://recap.dartconnect.com/games/666250706147440e89fcdbfb)
1. Jun 09 2024 5:54 PM: [301 SISO with KimD, SophiaR](https://recap.dartconnect.com/games/6666262d6147440e89001cbe)
1. Jun 09 2024 6:56 PM: [401 SISO with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/666635776147440e89002d96)
1. Jun 24 2024 7:01 PM: [401 SISO with AllieG, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6679fca8fab9ed7c6c7b8319)
1. Jun 24 2024 7:14 PM: [401 SISO with KatieR, KimD, ClothildeC, MeaganCL](https://recap.dartconnect.com/games/6679ffdefab9ed7c6c7b8860)
1. Jun 24 2024 8:38 PM: [301 SISO with MaryM, KimD](https://recap.dartconnect.com/games/667a12a0fab9ed7c6c7babf2)
1. Jun 24 2024 8:45 PM: [301 SISO with KimD, MaryM](https://recap.dartconnect.com/games/667a14c7fab9ed7c6c7bb03c)
1. Jul 01 2024 7:10 PM: [301 SISO with AllieG, KimD](https://recap.dartconnect.com/games/66833873fab9ed7c6c8411d4)
1. Jul 01 2024 7:48 PM: [401 SISO with KatieR, KimD, ClothildeC, DebbieR](https://recap.dartconnect.com/games/668341cefab9ed7c6c84221c)
1. Jul 01 2024 8:24 PM: [401 SISO with KimD, QuinnL](https://recap.dartconnect.com/games/66834a42fab9ed7c6c843280)
1. Jul 01 2024 8:55 PM: [501 SISO with KimD, QuinnL](https://recap.dartconnect.com/games/668351e3fab9ed7c6c8441fa)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
