# PamO

<title>PamO: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="PamO.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 5      | Marks Per Round: 0.96
| '01     | 2      | Points Per Round: 35.28
| Total   | 7      | Win Percentage: 57.14

#### Average Marks Per Round, Cricket: 0.96

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Jun 06 2024 6:21 PM: [Cricket with MeaganCL, QuinnL, KatieR, PamO](https://recap.dartconnect.com/games/66623baf6147440e89fcbeef)
1. Jun 06 2024 6:48 PM: [Cricket with KatieR, PamO, KimD, SylvieC](https://recap.dartconnect.com/games/666240906147440e89fcc433)
1. Jun 06 2024 7:21 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/666247a06147440e89fccdbd)
1. Jun 06 2024 7:39 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/66624b816147440e89fcd3a1)
1. Jun 06 2024 8:21 PM: [Cricket with PamO, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/666254e76147440e89fce370)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 35.28

<details><b><summary>Click for list of '01 matches</summary></b>

1. Jun 06 2024 7:06 PM: [501 SISO with KatieR, MeaganCL, PamO, SylvieC](https://recap.dartconnect.com/games/666242dc6147440e89fcc719)
1. Jun 06 2024 8:03 PM: [501 SISO with KatieR, MeaganCL, KimD, PamO](https://recap.dartconnect.com/games/666250706147440e89fcdbfb)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
