window.onload = function() {
    var Cricket = new CanvasJS.Chart("Cricket-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#4F81BC",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 0.9
            }, {
                x: 2,
                y: 0.9
            }, {
                x: 3,
                y: 0.6
            }, {
                x: 4,
                y: 0.8
            }, {
                x: 5,
                y: 0.5
            }, {
                x: 6,
                y: 0.8
            }, {
                x: 7,
                y: 0.6
            }, {
                x: 8,
                y: 0.8
            }, {
                x: 9,
                y: 1.1
            }, ],
        }, ],
        axisY: {
            title: "Marks Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 3,
        },
    });
    Cricket.render();
    var O1 = new CanvasJS.Chart("O1-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#C0504E",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 29.28
            }, {
                x: 2,
                y: 47.4
            }, {
                x: 3,
                y: 34.75
            }, {
                x: 4,
                y: 35.53
            }, {
                x: 5,
                y: 33.17
            }, {
                x: 6,
                y: 36.9
            }, {
                x: 7,
                y: 40.4
            }, {
                x: 8,
                y: 29.7
            }, {
                x: 9,
                y: 31.13
            }, {
                x: 10,
                y: 56.12
            }, {
                x: 11,
                y: 35.32
            }, ],
        }, ],
        axisY: {
            title: "Points Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 75,
        },
    });
    O1.render();
};