# AllieG

<title>AllieG: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="AllieG.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 51     | Marks Per Round: 0.91
| '01     | 31     | Points Per Round: 33.21
| Total   | 82     | Win Percentage: 26.83

#### Average Marks Per Round, Cricket: 0.91

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 25 2024 4:57 PM: [Cricket with AveryL, AllieG](https://recap.dartconnect.com/games/65dbbd9b95718706c3978be9)
1. Feb 25 2024 5:28 PM: [Cricket with AllieG, ClothildeC, KatieR, KimD](https://recap.dartconnect.com/games/65dbc30c95718706c397921a)
1. Feb 25 2024 6:43 PM: [Cricket with AveryL, ChristianaL, AllieG, MDD](https://recap.dartconnect.com/games/65dbd6cb95718706c397a96d)
1. Mar 10 2024 5:08 PM: [Cricket with AveryL, AllieG](https://recap.dartconnect.com/games/65ee264495718706c3a7e583)
1. Mar 10 2024 5:49 PM: [Cricket with AveryL, SophiaR, AllieG, ClothildeC](https://recap.dartconnect.com/games/65ee2dc095718706c3a7ee63)
1. Mar 10 2024 6:13 PM: [Cricket with AllieG, AnneR, ClothildeC, HannahCL](https://recap.dartconnect.com/games/65ee352495718706c3a7f7f4)
1. Mar 10 2024 7:01 PM: [Cricket with AllieG, ClothildeC](https://recap.dartconnect.com/games/65ee3e0995718706c3a80399)
1. Mar 10 2024 7:32 PM: [Cricket with AllieG, AveryL](https://recap.dartconnect.com/games/65ee489795718706c3a811d3)
1. Mar 24 2024 5:54 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600a6e60ed8913ccd2a27ce)
1. Mar 24 2024 6:22 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600abfb0ed8913ccd2a2e28)
1. Mar 24 2024 7:31 PM: [Cricket with AllieG, AveryL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600c0580ed8913ccd2a480f)
1. Apr 28 2024 5:25 PM: [Cricket with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec3916147440e89d92c65)
1. Apr 28 2024 6:07 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/662ecc6a6147440e89d9345c)
1. Apr 28 2024 6:39 PM: [Cricket with AllieG, ClothildeC](https://recap.dartconnect.com/games/662ed1e06147440e89d93997)
1. Apr 28 2024 7:32 PM: [Cricket with AllieG, KimD, KatieR, SophiaR](https://recap.dartconnect.com/games/662eded76147440e89d9471a)
1. Apr 28 2024 7:45 PM: [Cricket with AllieG, SophiaR](https://recap.dartconnect.com/games/662ee58a6147440e89d94dc4)
1. May 05 2024 5:15 PM: [Cricket with AllieG, AveryL](https://recap.dartconnect.com/games/6637fde06147440e89e10737)
1. May 05 2024 6:06 PM: [Cricket with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/6638059b6147440e89e10f02)
1. May 05 2024 6:32 PM: [Cricket with ClothildeC, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66380dd56147440e89e116c4)
1. May 05 2024 7:39 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/66381ca36147440e89e12614)
1. May 12 2024 6:47 PM: [Cricket with AllieG, KimD, AveryL, QuinnL](https://recap.dartconnect.com/games/66414b9d6147440e89e72fcc)
1. May 12 2024 7:13 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/66414fa56147440e89e73353)
1. May 19 2024 5:16 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/664a6f7d6147440e89eda36d)
1. May 19 2024 5:37 PM: [Cricket with AllieG, KimD, BeccaE, QuinnL](https://recap.dartconnect.com/games/664a74666147440e89eda7bb)
1. May 19 2024 6:37 PM: [Cricket with KimD, MeaganCL, AllieG, BeccaE](https://recap.dartconnect.com/games/664a88266147440e89edb96d)
1. May 26 2024 8:39 PM: [Cricket with AllieG, ClothildeC, AveryL, QuinnL](https://recap.dartconnect.com/games/6653db066147440e89f3842d)
1. May 26 2024 9:03 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e0e06147440e89f388f0)
1. May 26 2024 9:27 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e6616147440e89f38d63)
1. Jun 06 2024 7:41 PM: [Cricket with KimD, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66624cb16147440e89fcd5b7)
1. Jun 06 2024 7:58 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/66624fc36147440e89fcdad3)
1. Jun 06 2024 8:21 PM: [Cricket with AllieG, MeaganCL](https://recap.dartconnect.com/games/6662568e6147440e89fce614)
1. Jun 06 2024 8:58 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625cf56147440e89fcf0d3)
1. Jun 06 2024 9:06 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625f2d6147440e89fcf45b)
1. Jun 09 2024 5:22 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/666622406147440e89001862)
1. Jun 09 2024 5:46 PM: [Cricket with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666625b56147440e89001c32)
1. Jun 09 2024 6:10 PM: [Cricket with KatieR, SophiaR, AllieG, KimD](https://recap.dartconnect.com/games/66662bfe6147440e890022e4)
1. Jun 09 2024 6:44 PM: [Cricket with ClothildeC, KatieR, AllieG, QuinnL](https://recap.dartconnect.com/games/6666322f6147440e890029cd)
1. Jun 09 2024 6:56 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/666635546147440e89002d73)
1. Jun 09 2024 7:10 PM: [Cricket with AllieG, ClothildeC, QuinnL, SophiaR](https://recap.dartconnect.com/games/666639666147440e89003236)
1. Jun 24 2024 6:43 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/6679fa8bfab9ed7c6c7b7fe8)
1. Jun 24 2024 7:28 PM: [Cricket with AllieG, ClothildeC, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a035afab9ed7c6c7b8e91)
1. Jun 24 2024 7:42 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/667a0873fab9ed7c6c7b9873)
1. Jun 24 2024 8:08 PM: [Cricket with AllieG, KimD, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a0de2fab9ed7c6c7ba2ea)
1. Jul 01 2024 6:13 PM: [Cricket with DebbieR, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/66832c53fab9ed7c6c8401e1)
1. Jul 01 2024 6:36 PM: [Cricket with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/668332dcfab9ed7c6c840a24)
1. Jul 01 2024 7:25 PM: [Cricket with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/66833ef3fab9ed7c6c841cbc)
1. Jul 01 2024 7:45 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/668342bdfab9ed7c6c8423dd)
1. Jul 01 2024 8:01 PM: [Cricket with AllieG, KimD, DebbieR, QuinnL](https://recap.dartconnect.com/games/6683463efab9ed7c6c842a77)
1. Jul 01 2024 8:24 PM: [Cricket with AllieG, ClothildeC, DebbieR, KatieR](https://recap.dartconnect.com/games/66834c04fab9ed7c6c8435fa)
1. Jul 01 2024 8:54 PM: [Cricket with AllieG, ClothildeC, DebbieR, KatieR](https://recap.dartconnect.com/games/66835640fab9ed7c6c844a65)
1. Jul 01 2024 9:42 PM: [Cricket with AllieG, KimD, ClothildeC, QuinnL](https://recap.dartconnect.com/games/66835e3afab9ed7c6c8457de)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 33.21

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 25 2024 5:56 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbc84d95718706c39797d2)
1. Feb 25 2024 6:11 PM: [301 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65dbcb0695718706c3979b1d)
1. Mar 10 2024 5:31 PM: [401 SISO with AllieG, KimD, AveryL, SophiaR](https://recap.dartconnect.com/games/65ee29fb95718706c3a7e9ce)
1. Mar 10 2024 6:41 PM: [201 SISO with AllieG, KimD](https://recap.dartconnect.com/games/65ee386e95718706c3a7fc01)
1. Mar 10 2024 6:50 PM: [301 SISO with AllieG, AnneR, HannahCL, KimD](https://recap.dartconnect.com/games/65ee3b6095718706c3a7ff92)
1. Mar 24 2024 5:19 PM: [301 SISO with AllieG, AveryL](https://recap.dartconnect.com/games/66009c110ed8913ccd2a1a77)
1. Mar 24 2024 5:35 PM: [501 SISO with AveryL, QuinnL, AllieG, BeccaE](https://recap.dartconnect.com/games/6600a0b60ed8913ccd2a200d)
1. Mar 24 2024 6:43 PM: [501 SISO with AllieG, HannahCL, AveryL, MeaganCL](https://recap.dartconnect.com/games/6600b20d0ed8913ccd2a35fd)
1. Mar 24 2024 7:15 PM: [501 SISO with AveryL, JustineB, AllieG, KatieLan](https://recap.dartconnect.com/games/6600b6cf0ed8913ccd2a3c38)
1. Apr 28 2024 5:49 PM: [401 SISO with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec5bf6147440e89d92e47)
1. Apr 28 2024 5:58 PM: [501 SISO with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/662ec8356147440e89d9304b)
1. Apr 28 2024 7:12 PM: [501 SISO with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/662edb656147440e89d9435a)
1. May 05 2024 6:19 PM: [401 SISO with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/663808126147440e89e11159)
1. May 05 2024 6:57 PM: [401 SISO with AllieG, ClothildeC, MeaganCL, QuinnL](https://recap.dartconnect.com/games/6638116e6147440e89e11a63)
1. May 05 2024 7:22 PM: [501 SISO with AllieG, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/663817c76147440e89e120ca)
1. May 05 2024 7:58 PM: [301 SISO with AllieG, AveryL](https://recap.dartconnect.com/games/66381f6e6147440e89e128ec)
1. May 12 2024 5:39 PM: [301 SISO with AllieG, AveryL](https://recap.dartconnect.com/games/664138336147440e89e721c1)
1. May 12 2024 6:18 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/6641424c6147440e89e72891)
1. May 12 2024 6:28 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/664144cb6147440e89e72a48)
1. May 12 2024 7:49 PM: [401 SISO with AveryL, BeccaE, AllieG, QuinnL](https://recap.dartconnect.com/games/6641577f6147440e89e73a23)
1. May 19 2024 5:57 PM: [501 SISO with AllieG, QuinnL, BeccaE, KimD](https://recap.dartconnect.com/games/664a78f06147440e89edab7e)
1. May 19 2024 6:23 PM: [501 SISO with AllieG, HannahCL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/664a7eb26147440e89edb0df)
1. May 19 2024 7:17 PM: [501 SISO with AllieG, KimD, BeccaE, HannahCL](https://recap.dartconnect.com/games/664a8bda6147440e89edbcb5)
1. Jun 09 2024 6:01 PM: [401 SISO with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666627d06147440e89001e8a)
1. Jun 24 2024 7:01 PM: [401 SISO with AllieG, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6679fca8fab9ed7c6c7b8319)
1. Jun 24 2024 7:12 PM: [401 SISO with AllieG, MaryM, AnneR, QuinnL](https://recap.dartconnect.com/games/667a000dfab9ed7c6c7b88b5)
1. Jun 24 2024 8:43 PM: [401 SISO with AllieG, MeaganCL, AnneR, QuinnL](https://recap.dartconnect.com/games/667a15e1fab9ed7c6c7bb218)
1. Jun 24 2024 8:59 PM: [401 SISO with AllieG, ClothildeC](https://recap.dartconnect.com/games/667a17d4fab9ed7c6c7bb5b5)
1. Jun 24 2024 9:07 PM: [401 SISO with ClothildeC, AllieG](https://recap.dartconnect.com/games/667a19ccfab9ed7c6c7bb95a)
1. Jul 01 2024 7:10 PM: [301 SISO with AllieG, KimD](https://recap.dartconnect.com/games/66833873fab9ed7c6c8411d4)
1. Jul 01 2024 9:30 PM: [401 SISO with AllieG, ClothildeC](https://recap.dartconnect.com/games/66835a5afab9ed7c6c845189)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
