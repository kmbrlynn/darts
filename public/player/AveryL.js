window.onload = function() {
    var Cricket = new CanvasJS.Chart("Cricket-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#4F81BC",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 1.7
            }, {
                x: 2,
                y: 1.0
            }, {
                x: 3,
                y: 1.0
            }, {
                x: 4,
                y: 0.9
            }, {
                x: 5,
                y: 0.9
            }, {
                x: 6,
                y: 0.8
            }, {
                x: 7,
                y: 0.8
            }, {
                x: 8,
                y: 0.9
            }, {
                x: 9,
                y: 1.1
            }, {
                x: 10,
                y: 1.2
            }, {
                x: 11,
                y: 0.5
            }, {
                x: 12,
                y: 0.6
            }, {
                x: 13,
                y: 1.4
            }, {
                x: 14,
                y: 0.6
            }, {
                x: 15,
                y: 0.6
            }, {
                x: 16,
                y: 2.2
            }, {
                x: 17,
                y: 1.0
            }, {
                x: 18,
                y: 1.0
            }, {
                x: 19,
                y: 0.5
            }, {
                x: 20,
                y: 1.5
            }, {
                x: 21,
                y: 0.9
            }, ],
        }, ],
        axisY: {
            title: "Marks Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 3,
        },
    });
    Cricket.render();
    var O1 = new CanvasJS.Chart("O1-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#C0504E",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 21.75
            }, {
                x: 2,
                y: 37.25
            }, {
                x: 3,
                y: 34.5
            }, {
                x: 4,
                y: 29.43
            }, {
                x: 5,
                y: 28.22
            }, {
                x: 6,
                y: 33.13
            }, {
                x: 7,
                y: 25.7
            }, {
                x: 8,
                y: 48.17
            }, {
                x: 9,
                y: 40.6
            }, {
                x: 10,
                y: 24.75
            }, {
                x: 11,
                y: 24.41
            }, {
                x: 12,
                y: 56.44
            }, {
                x: 13,
                y: 29.33
            }, {
                x: 14,
                y: 32.83
            }, {
                x: 15,
                y: 29.0
            }, ],
        }, ],
        axisY: {
            title: "Points Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 75,
        },
    });
    O1.render();
};