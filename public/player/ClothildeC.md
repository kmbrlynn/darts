# ClothildeC

<title>ClothildeC: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="ClothildeC.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 42     | Marks Per Round: 1.87
| '01     | 28     | Points Per Round: 47.11
| Total   | 70     | Win Percentage: 71.43

#### Average Marks Per Round, Cricket: 1.87

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 7:28 PM: [Cricket with ClothildeC, QuinnL, KimD, MichaelaK](https://recap.dartconnect.com/games/65d2a36495718706c38f4fe6)
1. Feb 25 2024 5:28 PM: [Cricket with AllieG, ClothildeC, KatieR, KimD](https://recap.dartconnect.com/games/65dbc30c95718706c397921a)
1. Feb 25 2024 5:58 PM: [Cricket with ClothildeC, JesseyC, KatieR, QuinnL](https://recap.dartconnect.com/games/65dbcb1395718706c3979b2e)
1. Mar 10 2024 5:49 PM: [Cricket with AveryL, SophiaR, AllieG, ClothildeC](https://recap.dartconnect.com/games/65ee2dc095718706c3a7ee63)
1. Mar 10 2024 6:13 PM: [Cricket with AllieG, AnneR, ClothildeC, HannahCL](https://recap.dartconnect.com/games/65ee352495718706c3a7f7f4)
1. Mar 10 2024 7:01 PM: [Cricket with AllieG, ClothildeC](https://recap.dartconnect.com/games/65ee3e0995718706c3a80399)
1. Mar 24 2024 7:16 PM: [Cricket with ClothildeC, QuinnL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/6600b6e00ed8913ccd2a3c4a)
1. Mar 24 2024 7:49 PM: [Cricket with ClothildeC, MeaganCL, BeccaE, KimD](https://recap.dartconnect.com/games/6600bfe00ed8913ccd2a477a)
1. Mar 31 2024 6:44 PM: [Cricket with ClothildeC, KatieLiv, KatieR, KimD](https://recap.dartconnect.com/games/6609eb160ed8913ccd31721b)
1. Mar 31 2024 8:23 PM: [Cricket with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/660a029c0ed8913ccd318936)
1. Apr 06 2024 4:35 PM: [Cricket with ClothildeC, KatieR](https://recap.dartconnect.com/games/6611b4860ed8913ccd37c752)
1. Apr 06 2024 5:22 PM: [Cricket with ClothildeC, KimD](https://recap.dartconnect.com/games/6611bf990ed8913ccd37d5d1)
1. Apr 14 2024 6:03 PM: [Cricket with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c55b00ed8913ccd4040e1)
1. Apr 21 2024 5:29 PM: [Cricket with ClothildeC, KatieR](https://recap.dartconnect.com/games/6625890d6147440e89d28a5c)
1. Apr 21 2024 6:07 PM: [Cricket with KimD, ClothildeC](https://recap.dartconnect.com/games/6625901c6147440e89d29190)
1. Apr 21 2024 6:27 PM: [Cricket with KimD, ClothildeC](https://recap.dartconnect.com/games/6625952a6147440e89d296d8)
1. Apr 21 2024 7:28 PM: [Cricket with ClothildeC, KatieR, KatieLiv, SophiaR](https://recap.dartconnect.com/games/6625a64c6147440e89d2a946)
1. Apr 28 2024 6:10 PM: [Cricket with ClothildeC, KatieR](https://recap.dartconnect.com/games/662ecc166147440e89d9340f)
1. Apr 28 2024 6:39 PM: [Cricket with AllieG, ClothildeC](https://recap.dartconnect.com/games/662ed1e06147440e89d93997)
1. Apr 28 2024 7:31 PM: [Cricket with ClothildeC, QuinnL](https://recap.dartconnect.com/games/662ede7e6147440e89d9469a)
1. May 05 2024 6:06 PM: [Cricket with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/6638059b6147440e89e10f02)
1. May 05 2024 6:32 PM: [Cricket with ClothildeC, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66380dd56147440e89e116c4)
1. May 12 2024 5:15 PM: [Cricket with ClothildeC, KimD](https://recap.dartconnect.com/games/664133246147440e89e71e6a)
1. May 12 2024 6:00 PM: [Cricket with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413fa66147440e89e726c4)
1. May 12 2024 7:19 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664150706147440e89e733fb)
1. May 12 2024 7:29 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664154756147440e89e7375e)
1. May 23 2024 6:33 PM: [Cricket with ClothildeC, HannahCL, MeaganCL, QuinnL](https://recap.dartconnect.com/games/664fc99c6147440e89f11e57)
1. May 26 2024 8:39 PM: [Cricket with AllieG, ClothildeC, AveryL, QuinnL](https://recap.dartconnect.com/games/6653db066147440e89f3842d)
1. Jun 06 2024 7:21 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/666247a06147440e89fccdbd)
1. Jun 06 2024 7:39 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/66624b816147440e89fcd3a1)
1. Jun 06 2024 8:58 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625cf56147440e89fcf0d3)
1. Jun 06 2024 9:06 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625f2d6147440e89fcf45b)
1. Jun 09 2024 5:46 PM: [Cricket with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666625b56147440e89001c32)
1. Jun 09 2024 6:10 PM: [Cricket with ClothildeC, QuinnL](https://recap.dartconnect.com/games/66662a726147440e8900213c)
1. Jun 09 2024 6:44 PM: [Cricket with ClothildeC, KatieR, AllieG, QuinnL](https://recap.dartconnect.com/games/6666322f6147440e890029cd)
1. Jun 09 2024 7:10 PM: [Cricket with AllieG, ClothildeC, QuinnL, SophiaR](https://recap.dartconnect.com/games/666639666147440e89003236)
1. Jun 24 2024 6:46 PM: [Cricket with AnneR, ClothildeC, MaryM, MeaganCL](https://recap.dartconnect.com/games/6679fbb2fab9ed7c6c7b81b5)
1. Jun 24 2024 7:28 PM: [Cricket with AllieG, ClothildeC, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a035afab9ed7c6c7b8e91)
1. Jun 24 2024 8:07 PM: [Cricket with KatieR, MaryM, ClothildeC, QuinnL](https://recap.dartconnect.com/games/667a0e79fab9ed7c6c7ba405)
1. Jul 01 2024 8:24 PM: [Cricket with AllieG, ClothildeC, DebbieR, KatieR](https://recap.dartconnect.com/games/66834c04fab9ed7c6c8435fa)
1. Jul 01 2024 8:54 PM: [Cricket with AllieG, ClothildeC, DebbieR, KatieR](https://recap.dartconnect.com/games/66835640fab9ed7c6c844a65)
1. Jul 01 2024 9:42 PM: [Cricket with AllieG, KimD, ClothildeC, QuinnL](https://recap.dartconnect.com/games/66835e3afab9ed7c6c8457de)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 47.11

<details><b><summary>Click for list of '01 matches</summary></b>

1. Mar 24 2024 5:22 PM: [301 SIDO with KimD, ClothildeC](https://recap.dartconnect.com/games/66009b180ed8913ccd2a190b)
1. Mar 31 2024 5:55 PM: [401 SISO with ClothildeC, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6609dde00ed8913ccd316625)
1. Mar 31 2024 7:23 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, QuinnL](https://recap.dartconnect.com/games/6609f2390ed8913ccd317830)
1. Apr 14 2024 6:17 PM: [401 SISO with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c583a0ed8913ccd4043b3)
1. Apr 14 2024 6:32 PM: [401 SISO with ClothildeC, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/661c5bc90ed8913ccd404707)
1. Apr 14 2024 7:45 PM: [301 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/661c6c180ed8913ccd4057c0)
1. Apr 21 2024 6:47 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, SylvieC](https://recap.dartconnect.com/games/6625995c6147440e89d29b85)
1. Apr 21 2024 7:06 PM: [401 SISO with ClothildeC, KatieLiv](https://recap.dartconnect.com/games/66259db16147440e89d2a051)
1. Apr 21 2024 7:56 PM: [401 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/6625a9d16147440e89d2acc3)
1. May 05 2024 6:19 PM: [401 SISO with AllieG, AveryL, ClothildeC, SophiaR](https://recap.dartconnect.com/games/663808126147440e89e11159)
1. May 05 2024 6:57 PM: [401 SISO with AllieG, ClothildeC, MeaganCL, QuinnL](https://recap.dartconnect.com/games/6638116e6147440e89e11a63)
1. May 05 2024 7:22 PM: [401 SISO with ClothildeC, KatieLiv, AveryL, MeaganCL](https://recap.dartconnect.com/games/6638183c6147440e89e1213e)
1. May 05 2024 7:42 PM: [401 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/66381aac6147440e89e123da)
1. May 12 2024 5:44 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/664139e46147440e89e722e9)
1. May 12 2024 5:52 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413ba06147440e89e72419)
1. May 12 2024 6:18 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/6641424c6147440e89e72891)
1. May 12 2024 6:28 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/664144cb6147440e89e72a48)
1. May 12 2024 6:48 PM: [401 SIDO with ClothildeC, BeccaE](https://recap.dartconnect.com/games/664148d46147440e89e72d6e)
1. Jun 06 2024 7:00 PM: [401 SISO with ClothildeC, QuinnL](https://recap.dartconnect.com/games/666240c56147440e89fcc47e)
1. Jun 09 2024 6:01 PM: [401 SISO with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666627d06147440e89001e8a)
1. Jun 09 2024 6:56 PM: [401 SISO with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/666635776147440e89002d96)
1. Jun 24 2024 7:14 PM: [401 SISO with KatieR, KimD, ClothildeC, MeaganCL](https://recap.dartconnect.com/games/6679ffdefab9ed7c6c7b8860)
1. Jun 24 2024 8:59 PM: [401 SISO with AllieG, ClothildeC](https://recap.dartconnect.com/games/667a17d4fab9ed7c6c7bb5b5)
1. Jun 24 2024 9:07 PM: [401 SISO with ClothildeC, AllieG](https://recap.dartconnect.com/games/667a19ccfab9ed7c6c7bb95a)
1. Jul 01 2024 6:46 PM: [401 SISO with ClothildeC, SophiaR](https://recap.dartconnect.com/games/66833300fab9ed7c6c840a52)
1. Jul 01 2024 7:48 PM: [401 SISO with KatieR, KimD, ClothildeC, DebbieR](https://recap.dartconnect.com/games/668341cefab9ed7c6c84221c)
1. Jul 01 2024 7:58 PM: [401 SIDO with ClothildeC, KatieR](https://recap.dartconnect.com/games/668345d8fab9ed7c6c8429a3)
1. Jul 01 2024 9:30 PM: [401 SISO with AllieG, ClothildeC](https://recap.dartconnect.com/games/66835a5afab9ed7c6c845189)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
