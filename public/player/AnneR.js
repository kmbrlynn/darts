window.onload = function() {
    var Cricket = new CanvasJS.Chart("Cricket-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#4F81BC",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 0.9
            }, {
                x: 2,
                y: 1.0
            }, {
                x: 3,
                y: 0.7
            }, {
                x: 4,
                y: 0.5
            }, {
                x: 5,
                y: 0.3
            }, {
                x: 6,
                y: 0.5
            }, {
                x: 7,
                y: 0.7
            }, ],
        }, ],
        axisY: {
            title: "Marks Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 3,
        },
    });
    Cricket.render();
    var O1 = new CanvasJS.Chart("O1-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#C0504E",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 49.67
            }, {
                x: 2,
                y: 24.83
            }, {
                x: 3,
                y: 22.38
            }, {
                x: 4,
                y: 28.75
            }, {
                x: 5,
                y: 17.0
            }, {
                x: 6,
                y: 34.8
            }, {
                x: 7,
                y: 26.53
            }, ],
        }, ],
        axisY: {
            title: "Points Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 75,
        },
    });
    O1.render();
};