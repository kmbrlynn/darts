# BeccaE

<title>BeccaE: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="BeccaE.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 9      | Marks Per Round: 0.78
| '01     | 11     | Points Per Round: 37.25
| Total   | 20     | Win Percentage: 40.0

#### Average Marks Per Round, Cricket: 0.78

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Mar 24 2024 5:54 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600a6e60ed8913ccd2a27ce)
1. Mar 24 2024 6:22 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600abfb0ed8913ccd2a2e28)
1. Mar 24 2024 7:16 PM: [Cricket with ClothildeC, QuinnL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/6600b6e00ed8913ccd2a3c4a)
1. Mar 24 2024 7:49 PM: [Cricket with ClothildeC, MeaganCL, BeccaE, KimD](https://recap.dartconnect.com/games/6600bfe00ed8913ccd2a477a)
1. May 12 2024 6:00 PM: [Cricket with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413fa66147440e89e726c4)
1. May 12 2024 7:19 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664150706147440e89e733fb)
1. May 12 2024 7:29 PM: [Cricket with AveryL, ClothildeC, BeccaE, KimD](https://recap.dartconnect.com/games/664154756147440e89e7375e)
1. May 19 2024 5:37 PM: [Cricket with AllieG, KimD, BeccaE, QuinnL](https://recap.dartconnect.com/games/664a74666147440e89eda7bb)
1. May 19 2024 6:37 PM: [Cricket with KimD, MeaganCL, AllieG, BeccaE](https://recap.dartconnect.com/games/664a88266147440e89edb96d)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 37.25

<details><b><summary>Click for list of '01 matches</summary></b>

1. Mar 24 2024 5:35 PM: [501 SISO with AveryL, QuinnL, AllieG, BeccaE](https://recap.dartconnect.com/games/6600a0b60ed8913ccd2a200d)
1. May 12 2024 5:44 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/664139e46147440e89e722e9)
1. May 12 2024 5:52 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413ba06147440e89e72419)
1. May 12 2024 6:18 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/6641424c6147440e89e72891)
1. May 12 2024 6:28 PM: [401 SISO with AllieG, ClothildeC, AveryL, BeccaE](https://recap.dartconnect.com/games/664144cb6147440e89e72a48)
1. May 12 2024 6:48 PM: [401 SIDO with ClothildeC, BeccaE](https://recap.dartconnect.com/games/664148d46147440e89e72d6e)
1. May 12 2024 7:49 PM: [401 SISO with AveryL, BeccaE, AllieG, QuinnL](https://recap.dartconnect.com/games/6641577f6147440e89e73a23)
1. May 19 2024 5:16 PM: [301 SISO with KimD, BeccaE](https://recap.dartconnect.com/games/664a6e326147440e89eda263)
1. May 19 2024 5:57 PM: [501 SISO with AllieG, QuinnL, BeccaE, KimD](https://recap.dartconnect.com/games/664a78f06147440e89edab7e)
1. May 19 2024 6:23 PM: [501 SISO with AllieG, HannahCL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/664a7eb26147440e89edb0df)
1. May 19 2024 7:17 PM: [501 SISO with AllieG, KimD, BeccaE, HannahCL](https://recap.dartconnect.com/games/664a8bda6147440e89edbcb5)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
