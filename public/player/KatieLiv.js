window.onload = function() {
    var Cricket = new CanvasJS.Chart("Cricket-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#4F81BC",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 0.5
            }, {
                x: 2,
                y: 0.8
            }, {
                x: 3,
                y: 0.6
            }, {
                x: 4,
                y: 0.7
            }, {
                x: 5,
                y: 0.4
            }, {
                x: 6,
                y: 0.7
            }, {
                x: 7,
                y: 0.5
            }, {
                x: 8,
                y: 0.1
            }, {
                x: 9,
                y: 0.7
            }, ],
        }, ],
        axisY: {
            title: "Marks Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 3,
        },
    });
    Cricket.render();
    var O1 = new CanvasJS.Chart("O1-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#C0504E",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 31.0
            }, {
                x: 2,
                y: 40.0
            }, {
                x: 3,
                y: 21.17
            }, {
                x: 4,
                y: 44.5
            }, {
                x: 5,
                y: 37.4
            }, {
                x: 6,
                y: 34.67
            }, {
                x: 7,
                y: 22.25
            }, {
                x: 8,
                y: 22.14
            }, {
                x: 9,
                y: 32.4
            }, {
                x: 10,
                y: 34.55
            }, {
                x: 11,
                y: 29.8
            }, {
                x: 12,
                y: 38.14
            }, {
                x: 13,
                y: 28.75
            }, ],
        }, ],
        axisY: {
            title: "Points Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 75,
        },
    });
    O1.render();
};