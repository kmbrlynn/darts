# KatieLan

<title>KatieLan: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="KatieLan.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 1      | Marks Per Round: 0.5
| '01     | 2      | Points Per Round: 26.73
| Total   | 3      | Win Percentage: 33.33

#### Average Marks Per Round, Cricket: 0.5

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Mar 24 2024 6:38 PM: [Cricket with KatieLan, KimD, JustineB, SophiaR](https://recap.dartconnect.com/games/6600b14d0ed8913ccd2a3516)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 26.73

<details><b><summary>Click for list of '01 matches</summary></b>

1. Mar 24 2024 7:15 PM: [501 SISO with AveryL, JustineB, AllieG, KatieLan](https://recap.dartconnect.com/games/6600b6cf0ed8913ccd2a3c38)
1. Mar 24 2024 7:35 PM: [301 SISO with JustineB, KatieLan, MeaganCL, QuinnL](https://recap.dartconnect.com/games/6600bb740ed8913ccd2a423e)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
