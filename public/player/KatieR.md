# KatieR

<title>KatieR: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="KatieR.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 36     | Marks Per Round: 1.41
| '01     | 22     | Points Per Round: 42.15
| Total   | 58     | Win Percentage: 57.76

#### Average Marks Per Round, Cricket: 1.41

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 5:08 PM: [No Points with KatieR, KimD](https://recap.dartconnect.com/games/65d282d295718706c38f2520)
1. Feb 18 2024 5:24 PM: [Cricket with KatieR, KimD](https://recap.dartconnect.com/games/65d2868a95718706c38f29d3)
1. Feb 18 2024 5:40 PM: [Cricket with MegL, KatieR, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65d289f695718706c38f2e2e)
1. Feb 25 2024 5:09 PM: [Cricket with KatieR, KimD](https://recap.dartconnect.com/games/65dbbe5195718706c3978caf)
1. Feb 25 2024 5:28 PM: [Cricket with AllieG, ClothildeC, KatieR, KimD](https://recap.dartconnect.com/games/65dbc30c95718706c397921a)
1. Feb 25 2024 5:58 PM: [Cricket with ClothildeC, JesseyC, KatieR, QuinnL](https://recap.dartconnect.com/games/65dbcb1395718706c3979b2e)
1. Mar 31 2024 6:44 PM: [Cricket with ClothildeC, KatieLiv, KatieR, KimD](https://recap.dartconnect.com/games/6609eb160ed8913ccd31721b)
1. Mar 31 2024 8:00 PM: [Cricket with KatieR, SophiaR, KimD, QuinnL](https://recap.dartconnect.com/games/6609fd280ed8913ccd318339)
1. Mar 31 2024 8:23 PM: [Cricket with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/660a029c0ed8913ccd318936)
1. Apr 06 2024 4:35 PM: [Cricket with ClothildeC, KatieR](https://recap.dartconnect.com/games/6611b4860ed8913ccd37c752)
1. Apr 06 2024 4:56 PM: [Cricket with KimD, KatieR](https://recap.dartconnect.com/games/6611bcf80ed8913ccd37d264)
1. Apr 21 2024 5:29 PM: [Cricket with ClothildeC, KatieR](https://recap.dartconnect.com/games/6625890d6147440e89d28a5c)
1. Apr 21 2024 6:59 PM: [Cricket with SophiaR, SylvieC, KatieR, KimD](https://recap.dartconnect.com/games/6625a0106147440e89d2a2ad)
1. Apr 21 2024 7:28 PM: [Cricket with ClothildeC, KatieR, KatieLiv, SophiaR](https://recap.dartconnect.com/games/6625a64c6147440e89d2a946)
1. Apr 28 2024 5:25 PM: [Cricket with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec3916147440e89d92c65)
1. Apr 28 2024 6:10 PM: [Cricket with ClothildeC, KatieR](https://recap.dartconnect.com/games/662ecc166147440e89d9340f)
1. Apr 28 2024 7:32 PM: [Cricket with AllieG, KimD, KatieR, SophiaR](https://recap.dartconnect.com/games/662eded76147440e89d9471a)
1. Jun 06 2024 6:21 PM: [Cricket with MeaganCL, QuinnL, KatieR, PamO](https://recap.dartconnect.com/games/66623baf6147440e89fcbeef)
1. Jun 06 2024 6:48 PM: [Cricket with KatieR, PamO, KimD, SylvieC](https://recap.dartconnect.com/games/666240906147440e89fcc433)
1. Jun 06 2024 7:21 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/666247a06147440e89fccdbd)
1. Jun 06 2024 7:39 PM: [Cricket with ClothildeC, PamO, KatieR, SylvieC](https://recap.dartconnect.com/games/66624b816147440e89fcd3a1)
1. Jun 06 2024 8:21 PM: [Cricket with PamO, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/666254e76147440e89fce370)
1. Jun 09 2024 5:22 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/666622406147440e89001862)
1. Jun 09 2024 5:46 PM: [Cricket with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666625b56147440e89001c32)
1. Jun 09 2024 6:10 PM: [Cricket with KatieR, SophiaR, AllieG, KimD](https://recap.dartconnect.com/games/66662bfe6147440e890022e4)
1. Jun 09 2024 6:44 PM: [Cricket with ClothildeC, KatieR, AllieG, QuinnL](https://recap.dartconnect.com/games/6666322f6147440e890029cd)
1. Jun 09 2024 7:10 PM: [Cricket with KimD, KatieR](https://recap.dartconnect.com/games/666638b46147440e89003154)
1. Jun 24 2024 6:24 PM: [Cricket with KatieR, MeaganCL](https://recap.dartconnect.com/games/6679f57cfab9ed7c6c7b7921)
1. Jun 24 2024 6:43 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/6679fa8bfab9ed7c6c7b7fe8)
1. Jun 24 2024 7:27 PM: [Cricket with KimD, MaryM, KatieR, QuinnL](https://recap.dartconnect.com/games/667a0317fab9ed7c6c7b8e04)
1. Jun 24 2024 8:07 PM: [Cricket with KatieR, MaryM, ClothildeC, QuinnL](https://recap.dartconnect.com/games/667a0e79fab9ed7c6c7ba405)
1. Jul 01 2024 6:13 PM: [Cricket with DebbieR, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/66832c53fab9ed7c6c8401e1)
1. Jul 01 2024 6:36 PM: [Cricket with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/668332dcfab9ed7c6c840a24)
1. Jul 01 2024 7:25 PM: [Cricket with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/66833ef3fab9ed7c6c841cbc)
1. Jul 01 2024 8:24 PM: [Cricket with AllieG, ClothildeC, DebbieR, KatieR](https://recap.dartconnect.com/games/66834c04fab9ed7c6c8435fa)
1. Jul 01 2024 8:54 PM: [Cricket with AllieG, ClothildeC, DebbieR, KatieR](https://recap.dartconnect.com/games/66835640fab9ed7c6c844a65)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 42.15

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:38 PM: [401 SISO with KatieR, MichaelaK, MDD, QuinnL](https://recap.dartconnect.com/games/65d297d995718706c38f408b)
1. Feb 18 2024 7:30 PM: [401 SISO with AnneR, KatieR, MDD, MeaganCL](https://recap.dartconnect.com/games/65d2a4c995718706c38f51ab)
1. Feb 25 2024 6:27 PM: [401 SISO with QuinnL, SophiaR, KatieR, MichaelaK](https://recap.dartconnect.com/games/65dbd07795718706c397a1dc)
1. Mar 31 2024 5:55 PM: [401 SISO with ClothildeC, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6609dde00ed8913ccd316625)
1. Mar 31 2024 7:05 PM: [401 SISO with KatieLiv, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/6609ef380ed8913ccd3175ca)
1. Mar 31 2024 7:23 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, QuinnL](https://recap.dartconnect.com/games/6609f2390ed8913ccd317830)
1. Mar 31 2024 7:44 PM: [401 SISO with QuinnL, SophiaR, KatieLiv, KatieR](https://recap.dartconnect.com/games/6609f8820ed8913ccd317e77)
1. Apr 21 2024 6:04 PM: [401 SISO with KatieLiv, KatieR, StaceyE, SylvieC](https://recap.dartconnect.com/games/662590d16147440e89d29244)
1. Apr 21 2024 6:47 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, SylvieC](https://recap.dartconnect.com/games/6625995c6147440e89d29b85)
1. Apr 28 2024 5:49 PM: [401 SISO with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec5bf6147440e89d92e47)
1. Apr 28 2024 5:58 PM: [501 SISO with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/662ec8356147440e89d9304b)
1. Apr 28 2024 7:12 PM: [501 SISO with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/662edb656147440e89d9435a)
1. Apr 28 2024 7:45 PM: [401 SISO with KatieR, QuinnL](https://recap.dartconnect.com/games/662ee17f6147440e89d9499f)
1. Jun 06 2024 7:06 PM: [501 SISO with KatieR, MeaganCL, PamO, SylvieC](https://recap.dartconnect.com/games/666242dc6147440e89fcc719)
1. Jun 06 2024 8:03 PM: [501 SISO with KatieR, MeaganCL, KimD, PamO](https://recap.dartconnect.com/games/666250706147440e89fcdbfb)
1. Jun 09 2024 6:01 PM: [401 SISO with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666627d06147440e89001e8a)
1. Jun 09 2024 6:56 PM: [401 SISO with ClothildeC, SophiaR, KatieR, KimD](https://recap.dartconnect.com/games/666635776147440e89002d96)
1. Jun 24 2024 7:01 PM: [401 SISO with AllieG, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6679fca8fab9ed7c6c7b8319)
1. Jun 24 2024 7:14 PM: [401 SISO with KatieR, KimD, ClothildeC, MeaganCL](https://recap.dartconnect.com/games/6679ffdefab9ed7c6c7b8860)
1. Jun 24 2024 7:44 PM: [401 SISO with KatieR, MeaganCL, AnneR, MaryM](https://recap.dartconnect.com/games/667a0794fab9ed7c6c7b96a3)
1. Jul 01 2024 7:48 PM: [401 SISO with KatieR, KimD, ClothildeC, DebbieR](https://recap.dartconnect.com/games/668341cefab9ed7c6c84221c)
1. Jul 01 2024 7:58 PM: [401 SIDO with ClothildeC, KatieR](https://recap.dartconnect.com/games/668345d8fab9ed7c6c8429a3)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
