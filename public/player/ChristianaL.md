# ChristianaL

<title>ChristianaL: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="ChristianaL.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 1      | Marks Per Round: 0.6
| '01     | 1      | Points Per Round: 36.0
| Total   | 2      | Win Percentage: 100.0

#### Average Marks Per Round, Cricket: 0.6

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 25 2024 6:43 PM: [Cricket with AveryL, ChristianaL, AllieG, MDD](https://recap.dartconnect.com/games/65dbd6cb95718706c397a96d)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 36.0

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 25 2024 6:28 PM: [401 SISO with ChristianaL, KimD, AveryL, MDD](https://recap.dartconnect.com/games/65dbd01695718706c397a150)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
