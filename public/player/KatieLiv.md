# KatieLiv

<title>KatieLiv: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="KatieLiv.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 9      | Marks Per Round: 0.56
| '01     | 13     | Points Per Round: 32.06
| Total   | 22     | Win Percentage: 50.0

#### Average Marks Per Round, Cricket: 0.56

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Mar 31 2024 6:15 PM: [Cricket with KatieLiv, QuinnL](https://recap.dartconnect.com/games/6609e6470ed8913ccd316deb)
1. Mar 31 2024 6:44 PM: [Cricket with ClothildeC, KatieLiv, KatieR, KimD](https://recap.dartconnect.com/games/6609eb160ed8913ccd31721b)
1. Apr 14 2024 6:03 PM: [Cricket with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c55b00ed8913ccd4040e1)
1. Apr 14 2024 6:49 PM: [Cricket with QuinnL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/661c63920ed8913ccd404ed3)
1. Apr 14 2024 8:12 PM: [Cricket with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c74f80ed8913ccd406087)
1. Apr 21 2024 7:28 PM: [Cricket with ClothildeC, KatieR, KatieLiv, SophiaR](https://recap.dartconnect.com/games/6625a64c6147440e89d2a946)
1. May 05 2024 5:44 PM: [Cricket with MeaganCL, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/6638033a6147440e89e10ca0)
1. May 05 2024 6:37 PM: [Cricket with AveryL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/663813296147440e89e11c47)
1. May 26 2024 8:14 PM: [Cricket with KimD, SophiaR, AveryL, KatieLiv](https://recap.dartconnect.com/games/6653d5766147440e89f37f16)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 32.06

<details><b><summary>Click for list of '01 matches</summary></b>

1. Mar 31 2024 7:05 PM: [401 SISO with KatieLiv, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/6609ef380ed8913ccd3175ca)
1. Mar 31 2024 7:23 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, QuinnL](https://recap.dartconnect.com/games/6609f2390ed8913ccd317830)
1. Mar 31 2024 7:44 PM: [401 SISO with QuinnL, SophiaR, KatieLiv, KatieR](https://recap.dartconnect.com/games/6609f8820ed8913ccd317e77)
1. Apr 14 2024 6:17 PM: [401 SISO with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c583a0ed8913ccd4043b3)
1. Apr 14 2024 6:32 PM: [401 SISO with ClothildeC, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/661c5bc90ed8913ccd404707)
1. Apr 14 2024 7:20 PM: [401 SISO with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c675a0ed8913ccd405282)
1. Apr 14 2024 7:57 PM: [401 SISO with KatieLiv, QuinnL, KimD, SophiaR](https://recap.dartconnect.com/games/661c6f4f0ed8913ccd405ad5)
1. Apr 21 2024 6:04 PM: [401 SISO with KatieLiv, KatieR, StaceyE, SylvieC](https://recap.dartconnect.com/games/662590d16147440e89d29244)
1. Apr 21 2024 6:47 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, SylvieC](https://recap.dartconnect.com/games/6625995c6147440e89d29b85)
1. Apr 21 2024 7:06 PM: [401 SISO with ClothildeC, KatieLiv](https://recap.dartconnect.com/games/66259db16147440e89d2a051)
1. May 05 2024 5:31 PM: [301 SISO with KatieLiv, QuinnL](https://recap.dartconnect.com/games/6637fccf6147440e89e1063f)
1. May 05 2024 6:12 PM: [501 SISO with KatieLiv, QuinnL, KimD, MeaganCL](https://recap.dartconnect.com/games/663807ce6147440e89e11110)
1. May 05 2024 7:22 PM: [401 SISO with ClothildeC, KatieLiv, AveryL, MeaganCL](https://recap.dartconnect.com/games/6638183c6147440e89e1213e)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
