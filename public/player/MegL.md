# MegL

<title>MegL: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="MegL.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 2      | Marks Per Round: 1.35
| '01     | 1      | Points Per Round: 26.2
| Total   | 3      | Win Percentage: 33.33

#### Average Marks Per Round, Cricket: 1.35

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 5:40 PM: [Cricket with MegL, KatieR, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65d289f695718706c38f2e2e)
1. Feb 18 2024 6:04 PM: [Cricket with AnneR, MegL, MDD, MichaelaK](https://recap.dartconnect.com/games/65d2943e95718706c38f3bd4)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 26.2

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:53 PM: [401 SISO with AnneR, KimD, MeaganCL, MegL](https://recap.dartconnect.com/games/65d29b9d95718706c38f45b9)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
