# QuinnL

<title>QuinnL: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="QuinnL.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 59     | Marks Per Round: 1.24
| '01     | 38     | Points Per Round: 33.62
| Total   | 97     | Win Percentage: 51.03

#### Average Marks Per Round, Cricket: 1.24

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 5:40 PM: [Cricket with MegL, KatieR, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65d289f695718706c38f2e2e)
1. Feb 18 2024 7:28 PM: [Cricket with ClothildeC, QuinnL, KimD, MichaelaK](https://recap.dartconnect.com/games/65d2a36495718706c38f4fe6)
1. Feb 25 2024 5:58 PM: [Cricket with ClothildeC, JesseyC, KatieR, QuinnL](https://recap.dartconnect.com/games/65dbcb1395718706c3979b2e)
1. Mar 10 2024 6:13 PM: [Cricket with AveryL, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65ee371a95718706c3a7fa48)
1. Mar 10 2024 6:48 PM: [Cricket with MeaganCL, SophiaR, AveryL, QuinnL](https://recap.dartconnect.com/games/65ee412195718706c3a807da)
1. Mar 24 2024 5:54 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600a6e60ed8913ccd2a27ce)
1. Mar 24 2024 6:22 PM: [Cricket with AllieG, BeccaE, AveryL, QuinnL](https://recap.dartconnect.com/games/6600abfb0ed8913ccd2a2e28)
1. Mar 24 2024 7:16 PM: [Cricket with ClothildeC, QuinnL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/6600b6e00ed8913ccd2a3c4a)
1. Mar 31 2024 6:15 PM: [Cricket with KatieLiv, QuinnL](https://recap.dartconnect.com/games/6609e6470ed8913ccd316deb)
1. Mar 31 2024 8:00 PM: [Cricket with KatieR, SophiaR, KimD, QuinnL](https://recap.dartconnect.com/games/6609fd280ed8913ccd318339)
1. Apr 14 2024 5:37 PM: [Cricket with QuinnL, KimD](https://recap.dartconnect.com/games/661c51610ed8913ccd403c7a)
1. Apr 14 2024 6:03 PM: [Cricket with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c55b00ed8913ccd4040e1)
1. Apr 14 2024 6:49 PM: [Cricket with QuinnL, SophiaR, KatieLiv, KimD](https://recap.dartconnect.com/games/661c63920ed8913ccd404ed3)
1. Apr 14 2024 8:12 PM: [Cricket with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c74f80ed8913ccd406087)
1. Apr 28 2024 5:25 PM: [Cricket with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec3916147440e89d92c65)
1. Apr 28 2024 6:07 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/662ecc6a6147440e89d9345c)
1. Apr 28 2024 6:39 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/662ed5b46147440e89d93da8)
1. Apr 28 2024 7:31 PM: [Cricket with ClothildeC, QuinnL](https://recap.dartconnect.com/games/662ede7e6147440e89d9469a)
1. May 05 2024 5:44 PM: [Cricket with MeaganCL, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/6638033a6147440e89e10ca0)
1. May 05 2024 6:32 PM: [Cricket with ClothildeC, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66380dd56147440e89e116c4)
1. May 05 2024 7:39 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/66381ca36147440e89e12614)
1. May 12 2024 6:00 PM: [Cricket with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413fa66147440e89e726c4)
1. May 12 2024 6:18 PM: [Cricket with QuinnL, KimD](https://recap.dartconnect.com/games/6641435e6147440e89e72936)
1. May 12 2024 6:47 PM: [Cricket with AllieG, KimD, AveryL, QuinnL](https://recap.dartconnect.com/games/66414b9d6147440e89e72fcc)
1. May 12 2024 7:13 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/66414fa56147440e89e73353)
1. May 19 2024 5:16 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/664a6f7d6147440e89eda36d)
1. May 19 2024 5:37 PM: [Cricket with AllieG, KimD, BeccaE, QuinnL](https://recap.dartconnect.com/games/664a74666147440e89eda7bb)
1. May 23 2024 6:33 PM: [Cricket with ClothildeC, HannahCL, MeaganCL, QuinnL](https://recap.dartconnect.com/games/664fc99c6147440e89f11e57)
1. May 23 2024 7:40 PM: [Cricket with QuinnL, KimD](https://recap.dartconnect.com/games/664fd6616147440e89f12db7)
1. May 26 2024 8:39 PM: [Cricket with AllieG, ClothildeC, AveryL, QuinnL](https://recap.dartconnect.com/games/6653db066147440e89f3842d)
1. May 26 2024 9:03 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e0e06147440e89f388f0)
1. May 26 2024 9:27 PM: [Cricket with AllieG, QuinnL, AveryL, KimD](https://recap.dartconnect.com/games/6653e6616147440e89f38d63)
1. Jun 06 2024 6:21 PM: [Cricket with MeaganCL, QuinnL, KatieR, PamO](https://recap.dartconnect.com/games/66623baf6147440e89fcbeef)
1. Jun 06 2024 7:41 PM: [Cricket with KimD, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66624cb16147440e89fcd5b7)
1. Jun 06 2024 7:58 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/66624fc36147440e89fcdad3)
1. Jun 06 2024 8:21 PM: [Cricket with PamO, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/666254e76147440e89fce370)
1. Jun 06 2024 8:58 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625cf56147440e89fcf0d3)
1. Jun 06 2024 9:06 PM: [Cricket with AllieG, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66625f2d6147440e89fcf45b)
1. Jun 09 2024 5:22 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/666622406147440e89001862)
1. Jun 09 2024 5:46 PM: [Cricket with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666625b56147440e89001c32)
1. Jun 09 2024 6:10 PM: [Cricket with ClothildeC, QuinnL](https://recap.dartconnect.com/games/66662a726147440e8900213c)
1. Jun 09 2024 6:44 PM: [Cricket with ClothildeC, KatieR, AllieG, QuinnL](https://recap.dartconnect.com/games/6666322f6147440e890029cd)
1. Jun 09 2024 6:56 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/666635546147440e89002d73)
1. Jun 09 2024 7:10 PM: [Cricket with AllieG, ClothildeC, QuinnL, SophiaR](https://recap.dartconnect.com/games/666639666147440e89003236)
1. Jun 24 2024 6:18 PM: [Cricket with MaryM, QuinnL, AnneR, KimD](https://recap.dartconnect.com/games/6679f4b6fab9ed7c6c7b7836)
1. Jun 24 2024 6:43 PM: [Cricket with KimD, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/6679fa8bfab9ed7c6c7b7fe8)
1. Jun 24 2024 7:27 PM: [Cricket with KimD, MaryM, KatieR, QuinnL](https://recap.dartconnect.com/games/667a0317fab9ed7c6c7b8e04)
1. Jun 24 2024 7:42 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/667a0873fab9ed7c6c7b9873)
1. Jun 24 2024 8:07 PM: [Cricket with KatieR, MaryM, ClothildeC, QuinnL](https://recap.dartconnect.com/games/667a0e79fab9ed7c6c7ba405)
1. Jun 24 2024 9:02 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/667a1a48fab9ed7c6c7bba4d)
1. Jun 24 2024 9:17 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/667a1eabfab9ed7c6c7bc203)
1. Jul 01 2024 6:13 PM: [Cricket with DebbieR, QuinnL, AllieG, KatieR](https://recap.dartconnect.com/games/66832c53fab9ed7c6c8401e1)
1. Jul 01 2024 6:36 PM: [Cricket with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/668332dcfab9ed7c6c840a24)
1. Jul 01 2024 6:57 PM: [Cricket with SophiaR, QuinnL](https://recap.dartconnect.com/games/66833830fab9ed7c6c841164)
1. Jul 01 2024 7:25 PM: [Cricket with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/66833ef3fab9ed7c6c841cbc)
1. Jul 01 2024 7:45 PM: [Cricket with AllieG, QuinnL](https://recap.dartconnect.com/games/668342bdfab9ed7c6c8423dd)
1. Jul 01 2024 8:01 PM: [Cricket with AllieG, KimD, DebbieR, QuinnL](https://recap.dartconnect.com/games/6683463efab9ed7c6c842a77)
1. Jul 01 2024 9:04 PM: [Cricket with KimD, QuinnL](https://recap.dartconnect.com/games/6683574afab9ed7c6c844c33)
1. Jul 01 2024 9:42 PM: [Cricket with AllieG, KimD, ClothildeC, QuinnL](https://recap.dartconnect.com/games/66835e3afab9ed7c6c8457de)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 33.62

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:38 PM: [401 SISO with KatieR, MichaelaK, MDD, QuinnL](https://recap.dartconnect.com/games/65d297d995718706c38f408b)
1. Feb 25 2024 6:27 PM: [401 SISO with QuinnL, SophiaR, KatieR, MichaelaK](https://recap.dartconnect.com/games/65dbd07795718706c397a1dc)
1. Mar 10 2024 5:45 PM: [301 SISO with HannahCL, QuinnL, AnneR, MeaganCL](https://recap.dartconnect.com/games/65ee2ec595718706c3a7efc5)
1. Mar 24 2024 5:35 PM: [501 SISO with AveryL, QuinnL, AllieG, BeccaE](https://recap.dartconnect.com/games/6600a0b60ed8913ccd2a200d)
1. Mar 24 2024 7:35 PM: [301 SISO with JustineB, KatieLan, MeaganCL, QuinnL](https://recap.dartconnect.com/games/6600bb740ed8913ccd2a423e)
1. Mar 31 2024 5:55 PM: [401 SISO with ClothildeC, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6609dde00ed8913ccd316625)
1. Mar 31 2024 7:05 PM: [401 SISO with KatieLiv, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/6609ef380ed8913ccd3175ca)
1. Mar 31 2024 7:23 PM: [401 SISO with ClothildeC, KatieLiv, KatieR, QuinnL](https://recap.dartconnect.com/games/6609f2390ed8913ccd317830)
1. Mar 31 2024 7:44 PM: [401 SISO with QuinnL, SophiaR, KatieLiv, KatieR](https://recap.dartconnect.com/games/6609f8820ed8913ccd317e77)
1. Apr 14 2024 5:27 PM: [301 SISO with QuinnL, KimD](https://recap.dartconnect.com/games/661c4c080ed8913ccd403763)
1. Apr 14 2024 6:17 PM: [401 SISO with ClothildeC, KatieLiv, KimD, QuinnL](https://recap.dartconnect.com/games/661c583a0ed8913ccd4043b3)
1. Apr 14 2024 6:32 PM: [401 SISO with ClothildeC, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/661c5bc90ed8913ccd404707)
1. Apr 14 2024 7:20 PM: [401 SISO with KatieLiv, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/661c675a0ed8913ccd405282)
1. Apr 14 2024 7:37 PM: [201 SISO with SophiaR, QuinnL](https://recap.dartconnect.com/games/661c69fe0ed8913ccd405551)
1. Apr 14 2024 7:57 PM: [401 SISO with KatieLiv, QuinnL, KimD, SophiaR](https://recap.dartconnect.com/games/661c6f4f0ed8913ccd405ad5)
1. Apr 28 2024 5:49 PM: [401 SISO with AllieG, KatieR, KimD, QuinnL](https://recap.dartconnect.com/games/662ec5bf6147440e89d92e47)
1. Apr 28 2024 5:58 PM: [501 SISO with AllieG, KimD, KatieR, QuinnL](https://recap.dartconnect.com/games/662ec8356147440e89d9304b)
1. Apr 28 2024 7:12 PM: [501 SISO with KatieR, SophiaR, AllieG, QuinnL](https://recap.dartconnect.com/games/662edb656147440e89d9435a)
1. Apr 28 2024 7:45 PM: [401 SISO with KatieR, QuinnL](https://recap.dartconnect.com/games/662ee17f6147440e89d9499f)
1. May 05 2024 5:20 PM: [301 SISO with QuinnL, KimD](https://recap.dartconnect.com/games/6637f9636147440e89e10321)
1. May 05 2024 5:31 PM: [301 SISO with KatieLiv, QuinnL](https://recap.dartconnect.com/games/6637fccf6147440e89e1063f)
1. May 05 2024 6:12 PM: [501 SISO with KatieLiv, QuinnL, KimD, MeaganCL](https://recap.dartconnect.com/games/663807ce6147440e89e11110)
1. May 05 2024 6:57 PM: [401 SISO with AllieG, ClothildeC, MeaganCL, QuinnL](https://recap.dartconnect.com/games/6638116e6147440e89e11a63)
1. May 05 2024 7:22 PM: [501 SISO with AllieG, KimD, QuinnL, SophiaR](https://recap.dartconnect.com/games/663817c76147440e89e120ca)
1. May 12 2024 5:44 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/664139e46147440e89e722e9)
1. May 12 2024 5:52 PM: [401 SISO with BeccaE, ClothildeC, KimD, QuinnL](https://recap.dartconnect.com/games/66413ba06147440e89e72419)
1. May 12 2024 7:49 PM: [401 SISO with AveryL, BeccaE, AllieG, QuinnL](https://recap.dartconnect.com/games/6641577f6147440e89e73a23)
1. May 19 2024 5:57 PM: [501 SISO with AllieG, QuinnL, BeccaE, KimD](https://recap.dartconnect.com/games/664a78f06147440e89edab7e)
1. May 23 2024 6:59 PM: [301 SISO with MeaganCL, QuinnL](https://recap.dartconnect.com/games/664fcbf76147440e89f120b8)
1. Jun 06 2024 6:49 PM: [401 SISO with MeaganCL, QuinnL](https://recap.dartconnect.com/games/66623eb16147440e89fcc1fc)
1. Jun 06 2024 7:00 PM: [401 SISO with ClothildeC, QuinnL](https://recap.dartconnect.com/games/666240c56147440e89fcc47e)
1. Jun 06 2024 7:08 PM: [401 SISO with QuinnL, KimD](https://recap.dartconnect.com/games/666244096147440e89fcc88c)
1. Jun 09 2024 6:01 PM: [401 SISO with AllieG, ClothildeC, KatieR, QuinnL](https://recap.dartconnect.com/games/666627d06147440e89001e8a)
1. Jun 24 2024 7:01 PM: [401 SISO with AllieG, QuinnL, KatieR, KimD](https://recap.dartconnect.com/games/6679fca8fab9ed7c6c7b8319)
1. Jun 24 2024 7:12 PM: [401 SISO with AllieG, MaryM, AnneR, QuinnL](https://recap.dartconnect.com/games/667a000dfab9ed7c6c7b88b5)
1. Jun 24 2024 8:43 PM: [401 SISO with AllieG, MeaganCL, AnneR, QuinnL](https://recap.dartconnect.com/games/667a15e1fab9ed7c6c7bb218)
1. Jul 01 2024 8:24 PM: [401 SISO with KimD, QuinnL](https://recap.dartconnect.com/games/66834a42fab9ed7c6c843280)
1. Jul 01 2024 8:55 PM: [501 SISO with KimD, QuinnL](https://recap.dartconnect.com/games/668351e3fab9ed7c6c8441fa)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
