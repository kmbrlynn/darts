window.onload = function() {
    var Cricket = new CanvasJS.Chart("Cricket-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#4F81BC",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 0.4
            }, {
                x: 2,
                y: 0.8
            }, {
                x: 3,
                y: 0.7
            }, {
                x: 4,
                y: 0.7
            }, {
                x: 5,
                y: 0.5
            }, {
                x: 6,
                y: 1.2
            }, {
                x: 7,
                y: 0.5
            }, {
                x: 8,
                y: 0.6
            }, {
                x: 9,
                y: 1.2
            }, {
                x: 10,
                y: 0.8
            }, {
                x: 11,
                y: 1.4
            }, {
                x: 12,
                y: 0.8
            }, {
                x: 13,
                y: 1.0
            }, {
                x: 14,
                y: 1.1
            }, {
                x: 15,
                y: 1.1
            }, {
                x: 16,
                y: 0.9
            }, {
                x: 17,
                y: 1.9
            }, {
                x: 18,
                y: 1.2
            }, ],
        }, ],
        axisY: {
            title: "Marks Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 3,
        },
    });
    Cricket.render();
    var O1 = new CanvasJS.Chart("O1-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#C0504E",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 43.83
            }, {
                x: 2,
                y: 29.17
            }, {
                x: 3,
                y: 13.38
            }, {
                x: 4,
                y: 34.73
            }, {
                x: 5,
                y: 22.0
            }, {
                x: 6,
                y: 30.0
            }, {
                x: 7,
                y: 23.0
            }, {
                x: 8,
                y: 24.33
            }, {
                x: 9,
                y: 25.0
            }, {
                x: 10,
                y: 30.5
            }, {
                x: 11,
                y: 35.29
            }, {
                x: 12,
                y: 29.63
            }, {
                x: 13,
                y: 44.14
            }, {
                x: 14,
                y: 30.0
            }, {
                x: 15,
                y: 33.4
            }, {
                x: 16,
                y: 20.2
            }, {
                x: 17,
                y: 35.17
            }, ],
        }, ],
        axisY: {
            title: "Points Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 75,
        },
    });
    O1.render();
};