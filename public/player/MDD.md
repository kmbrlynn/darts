# MDD

<title>MDD: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="MDD.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 3      | Marks Per Round: 0.9
| '01     | 3      | Points Per Round: 33.82
| Total   | 6      | Win Percentage: 25.0

#### Average Marks Per Round, Cricket: 0.9

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 5:33 PM: [Cricket with MDD, MichaelaK](https://recap.dartconnect.com/games/65d28b9695718706c38f3068)
1. Feb 18 2024 6:04 PM: [Cricket with AnneR, MegL, MDD, MichaelaK](https://recap.dartconnect.com/games/65d2943e95718706c38f3bd4)
1. Feb 25 2024 6:43 PM: [Cricket with AveryL, ChristianaL, AllieG, MDD](https://recap.dartconnect.com/games/65dbd6cb95718706c397a96d)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 33.82

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:38 PM: [401 SISO with KatieR, MichaelaK, MDD, QuinnL](https://recap.dartconnect.com/games/65d297d995718706c38f408b)
1. Feb 18 2024 7:30 PM: [401 SISO with AnneR, KatieR, MDD, MeaganCL](https://recap.dartconnect.com/games/65d2a4c995718706c38f51ab)
1. Feb 25 2024 6:28 PM: [401 SISO with ChristianaL, KimD, AveryL, MDD](https://recap.dartconnect.com/games/65dbd01695718706c397a150)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
