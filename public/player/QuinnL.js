window.onload = function() {
    var Cricket = new CanvasJS.Chart("Cricket-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#4F81BC",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 0.8
            }, {
                x: 2,
                y: 1.3
            }, {
                x: 3,
                y: 0.9
            }, {
                x: 4,
                y: 0.7
            }, {
                x: 5,
                y: 0.6
            }, {
                x: 6,
                y: 0.4
            }, {
                x: 7,
                y: 0.7
            }, {
                x: 8,
                y: 0.8
            }, {
                x: 9,
                y: 1.5
            }, {
                x: 10,
                y: 1.2
            }, {
                x: 11,
                y: 0.7
            }, {
                x: 12,
                y: 0.8
            }, {
                x: 13,
                y: 1.1
            }, {
                x: 14,
                y: 1.7
            }, {
                x: 15,
                y: 1.0
            }, {
                x: 16,
                y: 1.2
            }, {
                x: 17,
                y: 1.0
            }, {
                x: 18,
                y: 0.9
            }, {
                x: 19,
                y: 1.2
            }, {
                x: 20,
                y: 1.1
            }, {
                x: 21,
                y: 0.9
            }, {
                x: 22,
                y: 0.7
            }, {
                x: 23,
                y: 1.1
            }, {
                x: 24,
                y: 1.5
            }, {
                x: 25,
                y: 1.4
            }, {
                x: 26,
                y: 1.3
            }, {
                x: 27,
                y: 1.7
            }, {
                x: 28,
                y: 1.3
            }, {
                x: 29,
                y: 1.2
            }, {
                x: 30,
                y: 1.5
            }, {
                x: 31,
                y: 1.2
            }, {
                x: 32,
                y: 1.3
            }, {
                x: 33,
                y: 1.5
            }, {
                x: 34,
                y: 1.3
            }, {
                x: 35,
                y: 1.5
            }, {
                x: 36,
                y: 1.1
            }, {
                x: 37,
                y: 2.0
            }, {
                x: 38,
                y: 1.3
            }, {
                x: 39,
                y: 1.0
            }, {
                x: 40,
                y: 1.6
            }, {
                x: 41,
                y: 1.1
            }, {
                x: 42,
                y: 1.2
            }, {
                x: 43,
                y: 1.5
            }, {
                x: 44,
                y: 1.3
            }, {
                x: 45,
                y: 1.2
            }, {
                x: 46,
                y: 1.5
            }, {
                x: 47,
                y: 1.5
            }, {
                x: 48,
                y: 1.2
            }, {
                x: 49,
                y: 1.3
            }, {
                x: 50,
                y: 1.5
            }, {
                x: 51,
                y: 1.4
            }, {
                x: 52,
                y: 2.3
            }, {
                x: 53,
                y: 1.3
            }, {
                x: 54,
                y: 1.2
            }, {
                x: 55,
                y: 0.8
            }, {
                x: 56,
                y: 1.5
            }, {
                x: 57,
                y: 1.7
            }, {
                x: 58,
                y: 1.3
            }, {
                x: 59,
                y: 2.3
            }, ],
        }, ],
        axisY: {
            title: "Marks Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 3,
        },
    });
    Cricket.render();
    var O1 = new CanvasJS.Chart("O1-container", {
        animationEnabled: true,
        interactivityEnabled: true,
        zoomEnabled: true,
        axisX: {
            title: "Match number",
            titleFontSize: 20,
            labelFontSize: 14,
            interval: 2,
            minimum: 0,
            labelAutoFit: false,
            scaleBreaks: {
                autoCalculate: true,
                type: "straight"
            },
        },
        data: [{
            type: "spline",
            color: "#C0504E",
            xValueFormatString: "Match ###",
            dataPoints: [{
                x: 1,
                y: 25.75
            }, {
                x: 2,
                y: 29.71
            }, {
                x: 3,
                y: 15.0
            }, {
                x: 4,
                y: 23.89
            }, {
                x: 5,
                y: 25.17
            }, {
                x: 6,
                y: 33.2
            }, {
                x: 7,
                y: 30.33
            }, {
                x: 8,
                y: 39.0
            }, {
                x: 9,
                y: 30.0
            }, {
                x: 10,
                y: 27.0
            }, {
                x: 11,
                y: 21.0
            }, {
                x: 12,
                y: 36.17
            }, {
                x: 13,
                y: 28.83
            }, {
                x: 14,
                y: 24.13
            }, {
                x: 15,
                y: 32.5
            }, {
                x: 16,
                y: 36.0
            }, {
                x: 17,
                y: 50.08
            }, {
                x: 18,
                y: 32.38
            }, {
                x: 19,
                y: 40.67
            }, {
                x: 20,
                y: 41.05
            }, {
                x: 21,
                y: 29.13
            }, {
                x: 22,
                y: 33.43
            }, {
                x: 23,
                y: 36.0
            }, {
                x: 24,
                y: 30.88
            }, {
                x: 25,
                y: 43.0
            }, {
                x: 26,
                y: 38.0
            }, {
                x: 27,
                y: 33.8
            }, {
                x: 28,
                y: 22.14
            }, {
                x: 29,
                y: 37.63
            }, {
                x: 30,
                y: 48.12
            }, {
                x: 31,
                y: 48.12
            }, {
                x: 32,
                y: 30.0
            }, {
                x: 33,
                y: 33.8
            }, {
                x: 34,
                y: 38.0
            }, {
                x: 35,
                y: 52.67
            }, {
                x: 36,
                y: 33.29
            }, {
                x: 37,
                y: 30.0
            }, {
                x: 38,
                y: 37.73
            }, ],
        }, ],
        axisY: {
            title: "Points Per Round",
            titleFontSize: 20,
            labelFontSize: 14,
            minimum: 0,
            maximum: 75,
        },
    });
    O1.render();
};