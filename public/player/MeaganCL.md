# MeaganCL

<title>MeaganCL: Darts at Rocks</title>
<link rel='icon' type='image/png' href='../favicon-32x32.png'>
<script type="text/javascript" src="MeaganCL.js"></script>

[< Back to leaderboard](../index.html)

### Overview

| Game    | Played | Metric Average               
|---------|--------|------------------------------
| Cricket | 18     | Marks Per Round: 0.93
| '01     | 17     | Points Per Round: 29.63
| Total   | 35     | Win Percentage: 45.71

#### Average Marks Per Round, Cricket: 0.93

<details><b><summary>Click for list of Cricket matches</summary></b>

1. Feb 18 2024 5:40 PM: [Cricket with MegL, KatieR, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65d289f695718706c38f2e2e)
1. Mar 10 2024 6:13 PM: [Cricket with AveryL, MeaganCL, KimD, QuinnL](https://recap.dartconnect.com/games/65ee371a95718706c3a7fa48)
1. Mar 10 2024 6:48 PM: [Cricket with MeaganCL, SophiaR, AveryL, QuinnL](https://recap.dartconnect.com/games/65ee412195718706c3a807da)
1. Mar 24 2024 5:46 PM: [Cricket with KimD, MeaganCL, HannahCL, SophiaR](https://recap.dartconnect.com/games/6600a6bc0ed8913ccd2a2788)
1. Mar 24 2024 7:16 PM: [Cricket with ClothildeC, QuinnL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/6600b6e00ed8913ccd2a3c4a)
1. Mar 24 2024 7:49 PM: [Cricket with ClothildeC, MeaganCL, BeccaE, KimD](https://recap.dartconnect.com/games/6600bfe00ed8913ccd2a477a)
1. May 05 2024 5:44 PM: [Cricket with MeaganCL, QuinnL, KatieLiv, KimD](https://recap.dartconnect.com/games/6638033a6147440e89e10ca0)
1. May 05 2024 6:32 PM: [Cricket with ClothildeC, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66380dd56147440e89e116c4)
1. May 19 2024 5:37 PM: [Cricket with HannahCL, MeaganCL](https://recap.dartconnect.com/games/664a79196147440e89edaba7)
1. May 19 2024 6:37 PM: [Cricket with KimD, MeaganCL, AllieG, BeccaE](https://recap.dartconnect.com/games/664a88266147440e89edb96d)
1. May 23 2024 6:33 PM: [Cricket with ClothildeC, HannahCL, MeaganCL, QuinnL](https://recap.dartconnect.com/games/664fc99c6147440e89f11e57)
1. Jun 06 2024 6:21 PM: [Cricket with MeaganCL, QuinnL, KatieR, PamO](https://recap.dartconnect.com/games/66623baf6147440e89fcbeef)
1. Jun 06 2024 7:41 PM: [Cricket with KimD, MeaganCL, AllieG, QuinnL](https://recap.dartconnect.com/games/66624cb16147440e89fcd5b7)
1. Jun 06 2024 8:21 PM: [Cricket with AllieG, MeaganCL](https://recap.dartconnect.com/games/6662568e6147440e89fce614)
1. Jun 24 2024 6:24 PM: [Cricket with KatieR, MeaganCL](https://recap.dartconnect.com/games/6679f57cfab9ed7c6c7b7921)
1. Jun 24 2024 6:46 PM: [Cricket with AnneR, ClothildeC, MaryM, MeaganCL](https://recap.dartconnect.com/games/6679fbb2fab9ed7c6c7b81b5)
1. Jun 24 2024 7:28 PM: [Cricket with AllieG, ClothildeC, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a035afab9ed7c6c7b8e91)
1. Jun 24 2024 8:08 PM: [Cricket with AllieG, KimD, AnneR, MeaganCL](https://recap.dartconnect.com/games/667a0de2fab9ed7c6c7ba2ea)

</details>

This plots your average marks earned per round of three darts thrown during
games of Cricket. The highest possible is 9 marks 
(where all 3 darts hit a triple)

<div id="Cricket-container" style="width:100%; height:300px;"></div>

#### Average Points Per Round, '01: 29.63

<details><b><summary>Click for list of '01 matches</summary></b>

1. Feb 18 2024 6:53 PM: [401 SISO with AnneR, KimD, MeaganCL, MegL](https://recap.dartconnect.com/games/65d29b9d95718706c38f45b9)
1. Feb 18 2024 7:30 PM: [401 SISO with AnneR, KatieR, MDD, MeaganCL](https://recap.dartconnect.com/games/65d2a4c995718706c38f51ab)
1. Mar 10 2024 5:45 PM: [301 SISO with HannahCL, QuinnL, AnneR, MeaganCL](https://recap.dartconnect.com/games/65ee2ec595718706c3a7efc5)
1. Mar 24 2024 6:19 PM: [301 SISO with SophiaR, MeaganCL](https://recap.dartconnect.com/games/6600a8ed0ed8913ccd2a2a5c)
1. Mar 24 2024 6:43 PM: [501 SISO with AllieG, HannahCL, AveryL, MeaganCL](https://recap.dartconnect.com/games/6600b20d0ed8913ccd2a35fd)
1. Mar 24 2024 7:35 PM: [301 SISO with JustineB, KatieLan, MeaganCL, QuinnL](https://recap.dartconnect.com/games/6600bb740ed8913ccd2a423e)
1. May 05 2024 6:12 PM: [501 SISO with KatieLiv, QuinnL, KimD, MeaganCL](https://recap.dartconnect.com/games/663807ce6147440e89e11110)
1. May 05 2024 6:57 PM: [401 SISO with AllieG, ClothildeC, MeaganCL, QuinnL](https://recap.dartconnect.com/games/6638116e6147440e89e11a63)
1. May 05 2024 7:22 PM: [401 SISO with ClothildeC, KatieLiv, AveryL, MeaganCL](https://recap.dartconnect.com/games/6638183c6147440e89e1213e)
1. May 19 2024 6:23 PM: [501 SISO with AllieG, HannahCL, BeccaE, MeaganCL](https://recap.dartconnect.com/games/664a7eb26147440e89edb0df)
1. May 23 2024 6:59 PM: [301 SISO with MeaganCL, QuinnL](https://recap.dartconnect.com/games/664fcbf76147440e89f120b8)
1. Jun 06 2024 6:49 PM: [401 SISO with MeaganCL, QuinnL](https://recap.dartconnect.com/games/66623eb16147440e89fcc1fc)
1. Jun 06 2024 7:06 PM: [501 SISO with KatieR, MeaganCL, PamO, SylvieC](https://recap.dartconnect.com/games/666242dc6147440e89fcc719)
1. Jun 06 2024 8:03 PM: [501 SISO with KatieR, MeaganCL, KimD, PamO](https://recap.dartconnect.com/games/666250706147440e89fcdbfb)
1. Jun 24 2024 7:14 PM: [401 SISO with KatieR, KimD, ClothildeC, MeaganCL](https://recap.dartconnect.com/games/6679ffdefab9ed7c6c7b8860)
1. Jun 24 2024 7:44 PM: [401 SISO with KatieR, MeaganCL, AnneR, MaryM](https://recap.dartconnect.com/games/667a0794fab9ed7c6c7b96a3)
1. Jun 24 2024 8:43 PM: [401 SISO with AllieG, MeaganCL, AnneR, QuinnL](https://recap.dartconnect.com/games/667a15e1fab9ed7c6c7bb218)

</details>

This plots your average points earned per round of three darts thrown
during games of '01. The highest possible is 180 
(where all 3 darts hit a triple 20)

<div id="O1-container" style="width:100%; height:300px;"></div>
