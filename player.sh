#!/bin/bash
#
# Pipeline: dartconnect.com --> grok_data.sh --> generate_site.sh
#
# This is a helper script for generating markdown, javascript, and html
# files for a given player
#

set -euo pipefail
source data.sh
source util.sh

function player_page_and_plots () {
    player="$1"

    # Calculate metrics for this player, outputting a javascript plot
    # and a markdown template for an html page
    cat $MATCH_DATA | grep -A3 -B3 $player | ./metrics.py $player \
            ${PUBLIC}/player/${player}.js > ${PUBLIC}/player/${player}.md

    # Error-check and lint the javascript file
    npx prettier --write ${PUBLIC}/player/${player}.js
    js-beautify --replace ${PUBLIC}/player/${player}.js

    # Convert the markdown file to html
    markdown2html ${PUBLIC}/player/${player}.md ${PUBLIC}/player/${player}.html
}

if [[ $# != 1 ]]; then
    echo "$0: Requires a single argument, the player name. Options:"
    echo ${PLAYERS[@]}
    exit 1
fi

player_page_and_plots $1
