#!/bin/bash
#
# Pipeline: dartconnect.com --> grok_data.sh --> generate_site.sh
#
# Common utilities and variables

RAW_DATA=raw_data
PUBLIC=public

MATCH_DATA=match_data.txt

CSS="https://cdn.jsdelivr.net/npm/sakura.css/css/sakura.css"
CANVASJS="https://cdn.canvasjs.com/canvasjs.min.js"

function markdown2html () {
    input=$1
    output=$2
    echo "<!DOCTYPE html>" > $output
    echo "<link rel=\"stylesheet\" href=\"${CSS}\" />" >> $output
    echo "<script src=\"${CANVASJS}\"></script>" >> $output

    base=$(basename ${1})
    pandoc $input -o /tmp/${base}.html && cat /tmp/${base}.html >> $output
    echo "</html>" >> $output
    rm /tmp/${base}.html

    echo "$FUNCNAME: $input --> $output"
}
