#!/bin/python3
import sys
from datetime import datetime

# TODO put all these in dicts
DATE_INDEX = 1
GAME_INDEX = 2
NAME_INDEX = 3
MPR_INDEX = 4
TDA_INDEX = 5
WIN_INDEX = 6

MPR_STR = "Marks Per Round"
TDA_STR = "Points Per Round"
WIN_STR = "Win Percentage"


def players_array(l):
    players_str = l.split(": ")[1].strip("\n")
    players = []
    for i in range(0, len(players_str.split())):
        players.append(f"{players_str.split()[i]}")
    return players


def metrics_array(l):
    values_str = l.split(": ")[1].strip("\n")
    if values_str == "N/A":
        return None
    else:
        values = []
        for i in range(0, len(values_str.split())):
            values.append(float(values_str.split()[i]))
        return values


def average(array):
    if len(array) == 0:
        return "N/A"
    else:
        return round(sum(array) / len(array), 2)


def table_entry(game_type, stat_type, stat_for_match):
    avg = average(stat_for_match)
    print(
        "| {} |{:< 7} | {}: {}".format(
            game_type.ljust(7), len(stat_for_match), stat_type, avg
        )
    )


def stdin_match_data():
    with open(0) as f:
        matches = []
        for line in f:
            if line.startswith("https"):
                new_match = True
                match = []
            if new_match is True:
                if line.startswith("https"):
                    match.append(line.strip("\n"))
                if line.startswith("match_date"):
                    datestr = line.split(": ")[1].strip("\n")
                    match.append(datetime.strptime(datestr, "%a, %d-%b-%Y, %I:%M %p"))
                if line.startswith("match_type"):
                    match.append(line.split(": ")[1].strip("\n"))
                if line.startswith("match_players"):
                    match.append(players_array(line))
                if line.startswith("marks_per") or line.startswith("three_dart"):
                    match.append(metrics_array(line))
                if line.startswith("win_percentage"):
                    match.append(metrics_array(line))
                    matches.append(match)
                    new_match = False
        return sorted(matches, key=lambda m: (m[DATE_INDEX]))


def player_metrics(matches, name):
    marks_per_round = []
    three_dart_average = []
    win_percentage = []

    for m in matches:
        for i in range(0, len(m[NAME_INDEX])):
            if m[NAME_INDEX][i] == name:
                player = i
        if m[MPR_INDEX] is not None:
            marks_per_round.append(m[MPR_INDEX][player])
        if m[TDA_INDEX] is not None:
            three_dart_average.append((m[TDA_INDEX][player]))
        if m[WIN_INDEX] is not None:
            win_percentage.append((m[WIN_INDEX][player]))

    return marks_per_round, three_dart_average, win_percentage


def print_stats_table(mpr, tda, wp):
    print("| Game    | Played | Metric Average               ")
    print("|---------|--------|------------------------------")
    table_entry("Cricket", MPR_STR, mpr)
    table_entry("'01", TDA_STR, tda)
    table_entry("Total", WIN_STR, wp)


def print_match_details(list, game_type):
    print(f"<details><b><summary>Click for list of {game_type} matches</summary></b>")
    print("")

    tokens = ["Cricket", "No Points"] if game_type == "Cricket" else ["01 SI", "01 DI"]
    for m in list:
        for i in tokens:
            if i in m[GAME_INDEX]:
                link = m[0].replace("/players/", "/games/")
                datestr = m[1].strftime("%b %d %Y %-I:%M %p")
                names = ", ".join(m[NAME_INDEX])
                print(f"1. {datestr}: [{m[GAME_INDEX]} with {names}]({link})")
    print("")
    print("</details>")
    print("")


def plot_metric_js(js, game_type, metric_str, metric_array, ylim, color):

    # Open a common plot object
    js.write(f"var {game_type} = new CanvasJS.Chart('{game_type}-container', {{")
    js.write(f"animationEnabled: true,")
    js.write(f"interactivityEnabled: true,")
    js.write(f"zoomEnabled: true,")
    js.write(f"axisX: {{")
    js.write(f"title: 'Match number',")
    js.write(f"titleFontSize: 20,")
    js.write(f"labelFontSize: 14,")
    js.write(f"interval: 2,")
    js.write(f"minimum: 0,")
    js.write(f"labelAutoFit: false,")
    js.write(f"scaleBreaks: {{ autoCalculate: true, type: 'straight' }} }},")
    js.write(f"data: [{{ type: 'spline',")

    # Set the color
    js.write(f"color: '{color}',")

    # And the tooltip units (for date, do "MMM D h:mm tt")
    js.write(f"xValueFormatString: 'Match ###',")

    # Write the data, TODO write the date vals somewhere?
    js.write(f"dataPoints: [")
    for i in range(0, len(metric_array)):
        js.write(f"{{ x: {i + 1}, y: {metric_array[i]} }},")
    js.write(f"]}}],")

    # Write the y-axis label
    js.write(f"axisY: {{\n")
    js.write(f"title: '{metric_str}',")
    js.write(f"titleFontSize: 20,")
    js.write(f"labelFontSize: 14,")
    js.write(f"minimum: {ylim[0]},")
    js.write(f"maximum: {ylim[1]} }}")

    # Close the object and render it
    js.write(f"}}); {game_type}.render();")

    print("")
    print(f'<div id="{game_type}-container" style="width:100%; height:300px;"></div>')


def main():

    # TODO argparsing
    # cat $MATCH_DATA | ./metrics.py KatieR public/player/KatieR.js
    name = sys.argv[1]
    js_output_file = sys.argv[2]

    matches = stdin_match_data()
    marks_per_round, three_dart_average, win_percentage = player_metrics(matches, name)

    # Write the title
    print(f"# {name}")
    print("")
    print(f"<title>{name}: Darts at Rocks</title>")
    print("<link rel='icon' type='image/png' href='../favicon-32x32.png'>")
    print(f'<script type="text/javascript" src="{name}.js"></script>')
    print("")
    print("[< Back to leaderboard](../index.html)")
    print("")

    # Metrics table
    print("### Overview")
    print("")
    print_stats_table(marks_per_round, three_dart_average, win_percentage)
    print("")

    # Plots for each metric
    js = open(js_output_file, "w+")
    js.write("window.onload = function () {\n")

    print(f"#### Average {MPR_STR}, Cricket: {average(marks_per_round)}")
    print("")
    print_match_details(matches, "Cricket")
    print("This plots your average marks earned per round of three darts thrown during")
    print("games of Cricket. The highest possible is 9 marks ")
    print("(where all 3 darts hit a triple)")
    plot_metric_js(js, "Cricket", MPR_STR, marks_per_round, [0, 3], "#4F81BC")

    print("")
    print(f"#### Average {TDA_STR}, '01: {average(three_dart_average)}")
    print("")
    print_match_details(matches, "'01")
    print("This plots your average points earned per round of three darts thrown")
    print("during games of '01. The highest possible is 180 ")
    print("(where all 3 darts hit a triple 20)")
    plot_metric_js(js, "O1", TDA_STR, three_dart_average, [0, 75], "#C0504E")

    """
    print("")
    print(f"#### Average {WIN_STR}, All time: {win_percentage}")
    print("")
    print("This plots your average win percentage per set of games in a match.")
    print("Usually we only play one game per match.")
    plot_metric_js(js, "Wins", WIN_STR, win_percentage, [0, 100], "#2E8B57")
    print("")
    """

    js.write("}\n")
    js.close()


if __name__ == "__main__":
    main()
