#!/bin/bash
#
# Pipeline: dartconnect.com --> grok_data.sh --> generate_site.sh
#
# This script takes a master input file and creates intermediary markdown
# and JavaScript files.
#
# Markdowns are then converted to html and saved under 'public', which is
# renderable as a GitLab Pages site via CI/CD pipeline

set -euo pipefail
source data.sh
source util.sh

function set_up_directories () {
    rm -rf $PUBLIC && mkdir -vp ${PUBLIC}/player

    echo -e "$FUNCNAME: done\n"
}

function leaderboard () {
    # Calculate the leaderboard for all players, outputting a javascript plot
    # and a markdown template for an html page
    ./leaderboard.py ${PUBLIC}/player \
        ${PUBLIC}/leaderboard.js >> ${PUBLIC}/index.md

    # Error-check and lint the javascript file
    npx prettier --write ${PUBLIC}/leaderboard.js
    js-beautify --replace ${PUBLIC}/leaderboard.js
}

function generate_player_pages () {
    if [[ ! -f $MATCH_DATA ]]; then
        echo "$FUNCNAME: please run 'grok_data.sh > $MATCH_DATA'"
        exit 1
    fi

    for ((i = 0; i < ${#PLAYERS[@]}; i++)); do
        ./player.sh ${PLAYERS[i]}
        echo
    done

    echo -e "$FUNCNAME: all players done\n"
}

function generate_index_page () {
    # Init the index page
    echo "# Women's+ Darts at Rocks" > ${PUBLIC}/index.md
    echo >> ${PUBLIC}/index.md
    echo "<title>Darts at Rocks</title>" >> ${PUBLIC}/index.md
    echo "<link rel='icon' type='image/png' href='favicon-32x32.png'>" >> ${PUBLIC}/index.md
    echo "<script type='text/javascript' src='leaderboard.js'></script>" >> ${PUBLIC}/index.md
    echo >> ${PUBLIC}/index.md

    # Players section
    echo "## Players" >> ${PUBLIC}/index.md
    echo >> ${PUBLIC}/index.md
    echo "<details><summary>Click for individual player stats</summary>" >> ${PUBLIC}/index.md
    echo >> ${PUBLIC}/index.md
    for ((i = 0; i < ${#PLAYERS[@]}; i++)); do
        player=${PLAYERS[i]}
        echo "- [$player](player/$player.html)" >> ${PUBLIC}/index.md
    done
    echo >> ${PUBLIC}/index.md
    echo "</details>" >> ${PUBLIC}/index.md

    # Leaderboard section
    echo -e "\n## Leaderboard\n" >> ${PUBLIC}/index.md
    leaderboard
    echo -e "<div id='leaderboard-container' style='width:100%; height:1200px;'></div>" >> ${PUBLIC}/index.md

    # Install the icon
    cp -v img/favicon-32x32.png ${PUBLIC}/

    # Convert the markdown file to html
    markdown2html ${PUBLIC}/index.md ${PUBLIC}/index.html
}

# ======== main

# Set up clean markdown and public directories
set_up_directories

# Convert $MATCH_DATA into separate markdown files per player, calculating
# individual player metrics and corresponding javascript plots along the way
generate_player_pages

# Convert all markdown to html for GitLab Pages consumption, intersplicing
# calculated player metrics and javascript leaderboard plot along the way
generate_index_page
