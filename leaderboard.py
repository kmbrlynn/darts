#!/bin/python3
import sys
import os


MPR_STR = "Marks Per Round"
TDA_STR = "Points Per Round"
WIN_STR = "Win Percentage"

MPR_INDEX = 0
TDA_INDEX = 1
WIN_INDEX = 2


def get_metric(l):
    val = l.split("|")[3].split(":")[1]
    if "N/A" in val:
        return 0
    else:
        return float(val)


def player_metrics(input_dir):
    players = {}
    for file in sorted(os.listdir(input_dir), reverse=True):
        if file.endswith(".md"):
            player_name = file.replace(".md", "")
            with open(os.path.join(input_dir, file)) as f:
                metrics = [None, None, None]
                for line in f:
                    if line.startswith("| Cricket"):
                        metrics[MPR_INDEX] = get_metric(line)
                    if line.startswith("| '01"):
                        metrics[TDA_INDEX] = get_metric(line)
                    if line.startswith("| Total"):
                        metrics[WIN_INDEX] = get_metric(line)
                players.update({player_name: metrics})
    return players


def plot_leaderboard_axis(js, axis_num, game_str, ylim, color):
    js.write(f"axisX: {{ labelFontSize: 20 }},")
    js.write(f"axis{axis_num}: {{")
    js.write(f"title: '{game_str}',")
    js.write(f"titleFontSize: 20,")
    js.write(f"labelFontSize: 14,")
    js.write(f"titleFontColor: '{color}',")
    js.write(f"lineColor: '{color}',")
    js.write(f"labelFontColor: '{color}',")
    js.write(f"tickColor: '{color}',")
    js.write(f"minimum: {ylim[0]},")
    js.write(f"maximum: {ylim[1]} }},")


def plot_leaderboard_data(js, players, axis_type, game_type, game_index):
    names = list(players.keys())

    js.write(f"{{ type: 'bar',")
    js.write(f"axisYType: '{axis_type}',")
    js.write(f"name: '{game_type}',")
    js.write(f"dataPoints: [")
    for name in names:
        js.write(f"{{ y: {players[name][game_index]}, label: '{name}' }},")
    js.write(f"]}},")


def main():

    # TODO argparsing
    # ./leaderboard.py markdown/player/ markdown/leaderboard.js
    input_dir = sys.argv[1]
    js_output = sys.argv[2]

    players = player_metrics(input_dir)

    # Open a common plot object
    js = open(js_output, "w+")
    js.write("window.onload = function () {")
    js.write("var Leaderboard = new CanvasJS.Chart('leaderboard-container', {")
    js.write("animationEnabled: true,")
    js.write("theme: 'light1',")
    js.write("toolTip: { shared: true },")
    js.write("height: 1024,")

    # Write the axis formatters
    plot_leaderboard_axis(js, "Y", MPR_STR, [0, 3], "#4F81BC")
    plot_leaderboard_axis(js, "Y2", TDA_STR, [0, 60], "#C0504E")

    # Write the data
    js.write("data: [")
    plot_leaderboard_data(js, players, "primary", "Cricket", MPR_INDEX)
    plot_leaderboard_data(js, players, "secondary", "01", TDA_INDEX)
    js.write("]")

    # Close it out and format it
    js.write("}); Leaderboard.render(); }")
    js.close()


if __name__ == "__main__":
    main()
